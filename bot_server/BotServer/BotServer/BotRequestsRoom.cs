﻿using MainServerApp.Communication;
using Newtonsoft.Json.Linq;
using System;
using WebSocketSharp;
using WebSocketSharp.Server;

namespace BotServer
{
    class BotRequestsRoom : WebSocketBehavior
    {
        protected override void OnMessage(MessageEventArgs e)
        {
            JToken token = JObject.Parse(e.Data);

            switch ((EMessageType)((int)token["Type"]))
            {
                case EMessageType.REQUEST_BOT_REQUEST: //TODO
                    BotEntitiesManager.RequestNewBot((string)token["TargetGameRoom"]);
                    break;

                default:
                    break;
            }
        }

        protected override void OnClose(CloseEventArgs e)
        {
            base.OnClose(e);
        }

        protected override void OnOpen()
        {
            base.OnOpen();
        }
    }
}
