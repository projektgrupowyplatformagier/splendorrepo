﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using WebSocketSharp.Server;

namespace BotServer
{
    class Program
    {
        static void Main(string[] args)
        {
            WebSocketServer wssv = new WebSocketServer(6767);     // Create websock server at port 6767
            wssv.AddWebSocketService<BotRequestsRoom>("/BotReq"); // Create room for requesting bots at /BotReq

            wssv.Start(); // Start server

            Console.WriteLine("Press Enter key to stop the server...");
            Console.ReadLine();

            wssv.Stop();
        }
    }
}
