﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BotServer.Communication
{
    class GameStartVoteResponse
    {
        public string Type;
        public bool Vote;

        public GameStartVoteResponse(bool vote)
        {
            Type = "GAME_START_VOTE";
            Vote = vote;
        }
    }
}
