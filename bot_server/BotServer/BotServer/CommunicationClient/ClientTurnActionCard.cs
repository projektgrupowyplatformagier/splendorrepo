﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BotServer.CommunicationClient
{
    class ClientTurnActionCard
    {
        public string Type;
        public string Action;
        public string CardTier;
        public string CardId;

        public ClientTurnActionCard(string action, string cardId, string cardTier)
        {
            Type = "TURN_ACTION";
            Action = action;
            CardId = cardId;
            CardTier = cardTier;
        }
    }
}
