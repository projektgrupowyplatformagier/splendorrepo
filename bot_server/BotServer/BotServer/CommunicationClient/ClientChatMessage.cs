﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BotServer.Communication
{
    class ClientChatMessage
    {
        public string Type;
        public string Message;

        public ClientChatMessage(string message)
        {
            Type = "CHAT";
            Message = message;
        }
    }
}
