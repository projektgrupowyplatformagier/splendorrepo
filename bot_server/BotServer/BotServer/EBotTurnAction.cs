﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BotServer
{
    enum EBotTurnAction
    {
        RESERVE_CARD,
        BUY_CARD,
        TAKE_MARKERS_MULTIPLE_KIND,
        TAKE_MARKERS_ONE_KIND,
        PASS
    }
}
