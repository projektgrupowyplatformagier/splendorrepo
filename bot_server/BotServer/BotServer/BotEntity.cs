﻿using BotServer.Communication;
using BotServer.CommunicationClient;
using BotServer.GameItemSets;
using MainServerApp.Communication;
using MainServerApp.GameItems;
using MainServerApp.Utils;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using WebSocketSharp;

namespace BotServer
{
    public class BotEntity
    {
        private string GameRoomAddress;
        private Thread BotMainThread;
        private bool InGame;
        private WebSocket WebSock;
        private GameDeck InternalGameDeck;
        private BotInventory InternalInventory;
        private static int BotNameIdx = 0;
        private static string[] BotNames = { "BOT-Dan", "BOT-Frank", "BOT-Joe", "BOT-Andrew", "BOT-Gary", "BOT-Anne", "BOT-Jerome", "BOT-Danny", "BOT-James" };
        private AristocratCard nextAristocratGoal = null;
        private DevelopmentCard nextCardToBuy = null;
        private DevelopmentCard cardToReserve = null;

        public BotEntity(string gameRoomAddress)
        {
            GameRoomAddress = gameRoomAddress;

            GameRoomAddress += "?bot_name=" + BotNames[BotNameIdx];

            BotNameIdx = (BotNameIdx + 1) % BotNames.Length;

            InternalGameDeck = new GameDeck();
            InternalInventory = new BotInventory();

            BotMainThread = new Thread(Run);
        }

        public void Run(object data)
        {
            if (data.GetType() != typeof(BotEntity))
                return;

            BotEntity be = (BotEntity)data;
            InGame = true;

            using (var ws = new WebSocket(GameRoomAddress))
            {
                ws.OnMessage += OnMessage;

                WebSock = ws;

                ws.Connect();

                SendHelloMessage();

                while (InGame && ws.IsAlive);

                ws.Close();
                be.NotifyDeath();
            }
        }

        public void SendHelloMessage()
        {
            ClientChatMessage msg = new ClientChatMessage("Hello Ladies and Gentlemen!");
            string outJson = Newtonsoft.Json.JsonConvert.SerializeObject(msg);
            WebSock.Send(outJson);
        }

        public void SendGlHf()
        {
            ClientChatMessage msg = new ClientChatMessage("Good Luck & Have Fun!");
            string outJson = Newtonsoft.Json.JsonConvert.SerializeObject(msg);
            WebSock.Send(outJson);
        }

        public void SendDebugMessage(string data)
        {
            ClientChatMessage msg = new ClientChatMessage("DEBUG: " + data);
            string outJson = Newtonsoft.Json.JsonConvert.SerializeObject(msg);
            WebSock.Send(outJson);
        }

        private void UpdateGameState(FullGameStateUpdateMessage fullGameUpdateMessage)
        {
            InternalGameDeck.ParseFullGameStateUpdate(fullGameUpdateMessage);
            InternalInventory.ParsePlayerInventory(fullGameUpdateMessage.PlayerInventory);
        }

        public void OnMessage(object sender, MessageEventArgs e)
        {
            JToken token = JObject.Parse(e.Data);
            string outJson;

            Console.WriteLine("Received: " + token.ToString());

            switch ((EMessageType)((int)token["Type"]))
            {
                case EMessageType.GAME_START_VOTE_REQUEST:
                    GameStartVoteResponse voteResponse = new GameStartVoteResponse(true);
                    outJson = Newtonsoft.Json.JsonConvert.SerializeObject(voteResponse);

                    WebSock.Send(outJson);
                    break;

                case EMessageType.FULL_GAME_STATE_UPDATE:
                    FullGameStateUpdateMessage receivedFullGameUpdateMessage = new FullGameStateUpdateMessage(token);
                    if (receivedFullGameUpdateMessage.UpdateReason == EFullGameStateUpdateReason.GAME_START.ToString())
                    {
                        SendGlHf();
                    }

                    UpdateGameState(receivedFullGameUpdateMessage);
                    break;

                case EMessageType.TURN_NOTIFICATION:
                    //TODO: Randomize something 
                    PerformRandomTurnAction();
                    break;

                case EMessageType.GAME_STATE_UPDATE: //Parial game state update
                    //TODO: Not implemented yet
                    break;

                default:
                    break;
            }
        }

        private void PerformRandomTurnAction()
        {
            // Check if there are any cards available to buy
            List<DevelopmentCard> cardsCardsAvailableToBuy = new List<DevelopmentCard>();

            cardsCardsAvailableToBuy.AddRange(InternalInventory.GetAffordableReservedCards());

            // Filter cards not affordable by bot
            foreach(DevelopmentCard card in InternalGameDeck.GetDevelopmentCards())
            {
                if (InternalInventory.CanAffordCard(card))
                {
                    cardsCardsAvailableToBuy.Add(card);
                }
            }

            List<EBotTurnAction> availableActions = new List<EBotTurnAction>(EnumUtils.GetValues<EBotTurnAction>());

            if (cardsCardsAvailableToBuy.Count == 0) availableActions.Remove(EBotTurnAction.BUY_CARD);
            if (InternalInventory.GetReservedCardsCount() >= 3) availableActions.Remove(EBotTurnAction.RESERVE_CARD);
            if (!InternalGameDeck.AreMarkersForOneKindAvailable()) availableActions.Remove(EBotTurnAction.TAKE_MARKERS_ONE_KIND);
            if (!InternalGameDeck.IsAnyMarkerAvailable()) availableActions.Remove(EBotTurnAction.TAKE_MARKERS_MULTIPLE_KIND);

            Random rnd = new Random();
            EBotTurnAction actionToPerform = availableActions[rnd.Next(availableActions.Count)];
            // Basic AI
            if(nextAristocratGoal == null)
            {
                nextAristocratGoal = InternalGameDeck.GetRandomAristocratCardToChase();
            }
            // Check if bot can buy any card needed by Aristocrat (only if needed card is not selected yet)
            for(int i = 0; i < cardsCardsAvailableToBuy.Count; i++)
            {
                if (InternalInventory.IsCardAlreadyPossessed(cardsCardsAvailableToBuy[i]))
                {
                    continue;
                }
                if (nextAristocratGoal.RequiredBonuses.ContainsKey(cardsCardsAvailableToBuy[i].GetProducedMarker()))
                {
                    nextCardToBuy = cardsCardsAvailableToBuy[i];
                    break;
                }
            }
            if (nextCardToBuy != null)
            {
                actionToPerform = EBotTurnAction.BUY_CARD;
            }
            else
            {
                bool shallBotReserveCard = rnd.NextDouble() >= 0.5;
                bool potentialAOneOrMoreMarkerColorsDecision = rnd.NextDouble() >= 0.5;
                // Randomize - reserve cards with wanted ProducedMarkers or take markers
                if (availableActions.Contains(EBotTurnAction.RESERVE_CARD) && shallBotReserveCard)
                {
                    foreach (DevelopmentCard potentialCardToReserve in InternalGameDeck.GetDevelopmentCards())
                    {
                        foreach (KeyValuePair<MarkerColor, int> requiredBonus in nextAristocratGoal.RequiredBonuses)
                        {
                            if (potentialCardToReserve.GetProducedMarker() == requiredBonus.Key)
                            {
                                cardToReserve = potentialCardToReserve;
                            }
                        }
                    }
                    actionToPerform = EBotTurnAction.RESERVE_CARD;
                }
                else if(availableActions.Contains(EBotTurnAction.TAKE_MARKERS_ONE_KIND) && availableActions.Contains(EBotTurnAction.TAKE_MARKERS_MULTIPLE_KIND))
                {
                    if (potentialAOneOrMoreMarkerColorsDecision)
                    {
                        actionToPerform = EBotTurnAction.TAKE_MARKERS_ONE_KIND;
                    }
                    else
                    {
                        actionToPerform = EBotTurnAction.TAKE_MARKERS_MULTIPLE_KIND;
                    }
                }
                else if (availableActions.Contains(EBotTurnAction.TAKE_MARKERS_ONE_KIND))
                {
                    actionToPerform = EBotTurnAction.TAKE_MARKERS_ONE_KIND;
                }
                else if (availableActions.Contains(EBotTurnAction.TAKE_MARKERS_MULTIPLE_KIND))
                {
                    actionToPerform = EBotTurnAction.TAKE_MARKERS_MULTIPLE_KIND;
                }
                else
                {
                    actionToPerform = availableActions[rnd.Next(availableActions.Count)];
                }
            }

            DevelopmentCard cardToTake;
            ClientTurnActionCard cardTurnAction;
            JObject otherTurnAction;
            JObject takenMarkersArray;
            string additionalMessage = "";
            string outJson;
            Dictionary<MarkerColor, int> markersTaken = new Dictionary<MarkerColor, int>();

            switch (actionToPerform)
            {
                case EBotTurnAction.BUY_CARD:
                    cardToTake = nextCardToBuy;

                    cardTurnAction = new ClientTurnActionCard("BUY_CARD", cardToTake.GetCardId(), cardToTake.GetCardTierIdx().ToString());
                    outJson = Newtonsoft.Json.JsonConvert.SerializeObject(cardTurnAction);
                    WebSock.Send(outJson);

                    SendDebugMessage("Bought card " + cardToTake.GetCardId());
                    InternalInventory.AddDevelopmentCard(cardToTake);
                    InternalGameDeck.RemoveDevelopmentCard(cardToTake);
                    nextCardToBuy = null;
                    if(nextCardToBuy==null)
                        Console.Out.WriteLine("Removed the card to buy");
                    break;

                case EBotTurnAction.RESERVE_CARD:
                    cardToTake = InternalGameDeck.GetRandomCardToReserve();

                    cardTurnAction = new ClientTurnActionCard("RESERVE_CARD", cardToTake.GetCardId(), cardToTake.GetCardTierIdx().ToString());
                    outJson = Newtonsoft.Json.JsonConvert.SerializeObject(cardTurnAction);
                    WebSock.Send(outJson);

                    SendDebugMessage("Reserved card " + cardToTake.GetCardId());
                    break;

                case EBotTurnAction.TAKE_MARKERS_ONE_KIND:
                case EBotTurnAction.TAKE_MARKERS_MULTIPLE_KIND:
                    if (actionToPerform == EBotTurnAction.TAKE_MARKERS_ONE_KIND)
                    {
                        markersTaken.Add(InternalGameDeck.GetRandomOneKindMarkerToTake(), 2);
                    }
                    else
                    {
                        foreach(MarkerColor marker in InternalGameDeck.GetRandomMultipleKindMarkersToTake())
                        {
                            markersTaken.Add(marker, 1);
                        }
                    }

                    otherTurnAction = JObject.Parse(@"{""Type"":""TURN_ACTION"",""Action"":""TAKE_MARKERS"", ""TakenMarkers"":{}}");
                    takenMarkersArray = otherTurnAction["TakenMarkers"] as JObject;

                    foreach(KeyValuePair<MarkerColor, int> marker in markersTaken)
                    {
                        additionalMessage += marker.Key.ToString() + ":" + marker.Value;
                        takenMarkersArray.Add(new JProperty(marker.Key.ToString(), marker.Value));
                    }

                    WebSock.Send(otherTurnAction.ToString());

                    SendDebugMessage("Taken markers " + additionalMessage);
                    break;

                case EBotTurnAction.PASS:
                    otherTurnAction = JObject.Parse(@"{""Type"":""TURN_ACTION"",""Action"":""PASS""}");
                    WebSock.Send(otherTurnAction.ToString());
                    SendDebugMessage("PASS");
                    break;
            }
        }

        public void NotifyDeath()
        {
            BotEntitiesManager.HandleBotDeath(this);
        }

        public void Start()
        {
            BotMainThread.Start(this);
        }

        public bool IsAlive()
        {
            return BotMainThread.IsAlive;
        }

        public void Abort()
        {
            BotMainThread.Abort();
        }

        public void Join()
        {
            BotMainThread.Join();
        }
    }
}
