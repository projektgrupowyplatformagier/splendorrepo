﻿using MainServerApp.Communication.DataTypes;
using MainServerApp.GameItems;
using MainServerApp.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BotServer.GameItemSets
{
    class BotInventory
    {
        private List<DevelopmentCard> DevelopmentCards;
        private List<DevelopmentCard> ReservedDevelopmentCards;
        private List<AristocratCard> AristocratCards;
        private Dictionary<MarkerColor, int> Markers;
        private Dictionary<MarkerColor, int> Bonuses;

        public BotInventory()
        {
            InitializeContainers();
        }

        private void InitializeContainers()
        {
            DevelopmentCards = new List<DevelopmentCard>();
            ReservedDevelopmentCards = new List<DevelopmentCard>();
            AristocratCards = new List<AristocratCard>();
            Markers = new Dictionary<MarkerColor, int>();
            Bonuses = new Dictionary<MarkerColor, int>();

            foreach (MarkerColor marker in EnumUtils.GetValues<MarkerColor>())
            {
                Markers.Add(marker, 0);
                Bonuses.Add(marker, 0);
            }
        }

        public void ParsePlayerInventory(PlayerInventoryEntity myInventoryEntity)
        {
            InitializeContainers();

            foreach (AristocratCardEntity ace in myInventoryEntity.AristocratCards)
            {
                AristocratCards.Add(ace.Parse());
            }

            foreach (DevelopmentCardEntity dce in myInventoryEntity.DevelopmentCards)
            {
                DevelopmentCards.Add(dce.Parse());
            }

            foreach (DevelopmentCardEntity rdce in myInventoryEntity.ReservedDevelopmentCards)
            {
                ReservedDevelopmentCards.Add(rdce.Parse());
            }

            foreach (MarkerEntity me in myInventoryEntity.Markers)
            {
                Markers[EnumUtils.GetEnumValueFromName<MarkerColor>(me.Name)] = me.Count;
            }
        }

        public bool CanAffordCard(DevelopmentCard devCard)
        {
            int requiredBlueMarkers, requiredGreenMarkers, requiredWhiteMarkers, requiredRedMarkers, requiredBlackMarkers;
            try
            {
                requiredBlueMarkers = devCard.GetConsumedMarkers()[MarkerColor.BlueMarker];
            }
            catch
            {
                requiredBlueMarkers = 0;
            }
            try
            {
                requiredGreenMarkers = devCard.GetConsumedMarkers()[MarkerColor.GreenMarker];
            }
            catch
            {
                requiredGreenMarkers = 0;
            }
            try
            {
                requiredWhiteMarkers = devCard.GetConsumedMarkers()[MarkerColor.WhiteMarker];
            }
            catch
            {
                requiredWhiteMarkers = 0;
            }
            try
            {
                requiredRedMarkers = devCard.GetConsumedMarkers()[MarkerColor.RedMarker];
            }
            catch
            {
                requiredRedMarkers = 0;
            }
            try
            {
                requiredBlackMarkers = devCard.GetConsumedMarkers()[MarkerColor.BlackMarker];
            }
            catch
            {
                requiredBlackMarkers = 0;
            }

            // Try without GoldMarkers
            if (requiredBlackMarkers <= GetBlackMarkersCount() &&
                requiredBlueMarkers <= GetBlueMarkersCount() &&
                requiredGreenMarkers <= GetGreenMarkersCount() &&
                requiredRedMarkers <= GetRedMarkersCount() &&
                requiredWhiteMarkers <= GetWhiteMarkersCount())
            {
                return true;
            }
            else
            {
                // Try with GoldMarkers
                switch (GetGoldMarkersCount())
                {
                    case 3:
                        // TBD: Figure out a more cleaver way of checking markers variants
                    case 2:
                        return (
                            (requiredBlackMarkers <= GetBlackMarkersCount() + 2 &&
                            requiredBlueMarkers <= GetBlueMarkersCount() &&
                            requiredGreenMarkers <= GetGreenMarkersCount() &&
                            requiredRedMarkers <= GetRedMarkersCount() &&
                            requiredWhiteMarkers <= GetWhiteMarkersCount())
                            ||
                            (requiredBlackMarkers <= GetBlackMarkersCount() + 1 &&
                            requiredBlueMarkers <= GetBlueMarkersCount() + 1 &&
                            requiredGreenMarkers <= GetGreenMarkersCount() &&
                            requiredRedMarkers <= GetRedMarkersCount() &&
                            requiredWhiteMarkers <= GetWhiteMarkersCount())
                            ||
                            (requiredBlackMarkers <= GetBlackMarkersCount() + 1 &&
                            requiredBlueMarkers <= GetBlueMarkersCount() &&
                            requiredGreenMarkers <= GetGreenMarkersCount() + 1 &&
                            requiredRedMarkers <= GetRedMarkersCount() &&
                            requiredWhiteMarkers <= GetWhiteMarkersCount())
                            ||
                            (requiredBlackMarkers <= GetBlackMarkersCount() + 1 &&
                            requiredBlueMarkers <= GetBlueMarkersCount() &&
                            requiredGreenMarkers <= GetGreenMarkersCount() &&
                            requiredRedMarkers <= GetRedMarkersCount() + 1 &&
                            requiredWhiteMarkers <= GetWhiteMarkersCount())
                            ||
                            (requiredBlackMarkers <= GetBlackMarkersCount() + 1 &&
                            requiredBlueMarkers <= GetBlueMarkersCount() &&
                            requiredGreenMarkers <= GetGreenMarkersCount() &&
                            requiredRedMarkers <= GetRedMarkersCount() &&
                            requiredWhiteMarkers <= GetWhiteMarkersCount() + 1)
                            ||
                            (requiredBlackMarkers <= GetBlackMarkersCount() &&
                            requiredBlueMarkers <= GetBlueMarkersCount() + 2 &&
                            requiredGreenMarkers <= GetGreenMarkersCount() &&
                            requiredRedMarkers <= GetRedMarkersCount() &&
                            requiredWhiteMarkers <= GetWhiteMarkersCount())
                            ||
                            (requiredBlackMarkers <= GetBlackMarkersCount() &&
                            requiredBlueMarkers <= GetBlueMarkersCount() + 1 &&
                            requiredGreenMarkers <= GetGreenMarkersCount() + 1 &&
                            requiredRedMarkers <= GetRedMarkersCount() &&
                            requiredWhiteMarkers <= GetWhiteMarkersCount())
                            ||
                            (requiredBlackMarkers <= GetBlackMarkersCount() &&
                            requiredBlueMarkers <= GetBlueMarkersCount() + 1 &&
                            requiredGreenMarkers <= GetGreenMarkersCount() &&
                            requiredRedMarkers <= GetRedMarkersCount() + 1 &&
                            requiredWhiteMarkers <= GetWhiteMarkersCount())
                            ||
                            (requiredBlackMarkers <= GetBlackMarkersCount() &&
                            requiredBlueMarkers <= GetBlueMarkersCount() + 1 &&
                            requiredGreenMarkers <= GetGreenMarkersCount() &&
                            requiredRedMarkers <= GetRedMarkersCount() &&
                            requiredWhiteMarkers <= GetWhiteMarkersCount() + 1)
                            ||
                            (requiredBlackMarkers <= GetBlackMarkersCount() &&
                            requiredBlueMarkers <= GetBlueMarkersCount() &&
                            requiredGreenMarkers <= GetGreenMarkersCount() + 2 &&
                            requiredRedMarkers <= GetRedMarkersCount() &&
                            requiredWhiteMarkers <= GetWhiteMarkersCount())
                            ||
                            (requiredBlackMarkers <= GetBlackMarkersCount() &&
                            requiredBlueMarkers <= GetBlueMarkersCount() &&
                            requiredGreenMarkers <= GetGreenMarkersCount() + 1 &&
                            requiredRedMarkers <= GetRedMarkersCount() + 1 &&
                            requiredWhiteMarkers <= GetWhiteMarkersCount())
                            ||
                            (requiredBlackMarkers <= GetBlackMarkersCount() &&
                            requiredBlueMarkers <= GetBlueMarkersCount() &&
                            requiredGreenMarkers <= GetGreenMarkersCount() + 1 &&
                            requiredRedMarkers <= GetRedMarkersCount() &&
                            requiredWhiteMarkers <= GetWhiteMarkersCount() + 1)
                            ||
                            (requiredBlackMarkers <= GetBlackMarkersCount() &&
                            requiredBlueMarkers <= GetBlueMarkersCount() &&
                            requiredGreenMarkers <= GetGreenMarkersCount() &&
                            requiredRedMarkers <= GetRedMarkersCount() + 2 &&
                            requiredWhiteMarkers <= GetWhiteMarkersCount())
                            ||
                            (requiredBlackMarkers <= GetBlackMarkersCount() &&
                            requiredBlueMarkers <= GetBlueMarkersCount() &&
                            requiredGreenMarkers <= GetGreenMarkersCount() &&
                            requiredRedMarkers <= GetRedMarkersCount() + 1 &&
                            requiredWhiteMarkers <= GetWhiteMarkersCount() + 1)
                            ||
                            (requiredBlackMarkers <= GetBlackMarkersCount() &&
                            requiredBlueMarkers <= GetBlueMarkersCount() &&
                            requiredGreenMarkers <= GetGreenMarkersCount() &&
                            requiredRedMarkers <= GetRedMarkersCount() &&
                            requiredWhiteMarkers <= GetWhiteMarkersCount() + 2)
                            );
                    case 1:
                        return (
                            (requiredBlackMarkers <= GetBlackMarkersCount() + 1 &&
                            requiredBlueMarkers <= GetBlueMarkersCount() &&
                            requiredGreenMarkers <= GetGreenMarkersCount() &&
                            requiredRedMarkers <= GetRedMarkersCount() &&
                            requiredWhiteMarkers <= GetWhiteMarkersCount())
                            ||
                            (requiredBlackMarkers <= GetBlackMarkersCount() &&
                            requiredBlueMarkers <= GetBlueMarkersCount() + 1 &&
                            requiredGreenMarkers <= GetGreenMarkersCount() &&
                            requiredRedMarkers <= GetRedMarkersCount() &&
                            requiredWhiteMarkers <= GetWhiteMarkersCount())
                            ||
                            (requiredBlackMarkers <= GetBlackMarkersCount() &&
                            requiredBlueMarkers <= GetBlueMarkersCount() &&
                            requiredGreenMarkers <= GetGreenMarkersCount() + 1 &&
                            requiredRedMarkers <= GetRedMarkersCount() &&
                            requiredWhiteMarkers <= GetWhiteMarkersCount())
                            ||
                            (requiredBlackMarkers <= GetBlackMarkersCount() &&
                            requiredBlueMarkers <= GetBlueMarkersCount() &&
                            requiredGreenMarkers <= GetGreenMarkersCount() &&
                            requiredRedMarkers <= GetRedMarkersCount() + 1 &&
                            requiredWhiteMarkers <= GetWhiteMarkersCount())
                            ||
                            (requiredBlackMarkers <= GetBlackMarkersCount() &&
                            requiredBlueMarkers <= GetBlueMarkersCount() &&
                            requiredGreenMarkers <= GetGreenMarkersCount() &&
                            requiredRedMarkers <= GetRedMarkersCount() &&
                            requiredWhiteMarkers <= GetWhiteMarkersCount() + 1)
                            );
                    case 0:
                    default:
                        return false;
                }
            }
        }

        public List<DevelopmentCard> GetAffordableReservedCards()
        {
            List<DevelopmentCard> affordableCards = new List<DevelopmentCard>();

            foreach (DevelopmentCard card in ReservedDevelopmentCards)
            {
                if (CanAffordCard(card)) affordableCards.Add(card);
            }

            return affordableCards;
        }

        public int GetReservedCardsCount()
        {
            return ReservedDevelopmentCards.Count;
        }

        private int GetGoldMarkersCount()
        {
            return Markers[MarkerColor.GoldMarker];
        }

        private int GetBlackMarkersCount()
        {
            return Markers[MarkerColor.BlackMarker] + GatherCertainMarkersFromDevCards(MarkerColor.BlackMarker);
        }
        private int GetBlueMarkersCount()
        {
            return Markers[MarkerColor.BlueMarker] + GatherCertainMarkersFromDevCards(MarkerColor.BlueMarker);
        }
        private int GetRedMarkersCount()
        {
            return Markers[MarkerColor.RedMarker] + GatherCertainMarkersFromDevCards(MarkerColor.RedMarker);
        }
        private int GetWhiteMarkersCount()
        {
            return Markers[MarkerColor.WhiteMarker] + GatherCertainMarkersFromDevCards(MarkerColor.WhiteMarker);
        }
        private int GetGreenMarkersCount()
        {
            return Markers[MarkerColor.GreenMarker] + GatherCertainMarkersFromDevCards(MarkerColor.GreenMarker);
        }
        private int GatherCertainMarkersFromDevCards(MarkerColor marketColor)
        {
            if (DevelopmentCards.Count > 0)
            {
                int markers = 0;
                foreach (DevelopmentCard developmentCard in DevelopmentCards)
                {
                    if (developmentCard.GetProducedMarker() == marketColor)
                    {
                        markers++;
                    }
                }
                return markers;
            }
            else
            {
                return 0;
            }
        }
        public void AddDevelopmentCard(DevelopmentCard developmentCard)
        {
            DevelopmentCards.Add(developmentCard);
            foreach(DevelopmentCard card in DevelopmentCards)
            {
                Console.Out.WriteLine(card.GetCardId());
            }
        }
        public bool IsCardAlreadyPossessed(DevelopmentCard developmentCard)
        {
            foreach(DevelopmentCard card in DevelopmentCards)
            {
                if(card.GetCardId() == developmentCard.GetCardId())
                {
                    return true;
                }
            }
            return false;
        }
    }
}
