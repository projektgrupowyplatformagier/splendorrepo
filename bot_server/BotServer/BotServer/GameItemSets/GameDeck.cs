﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MainServerApp.GameItems;
using MainServerApp.Communication;
using MainServerApp.Utils;
using MainServerApp.Communication.DataTypes;

namespace BotServer.GameItemSets
{
    class GameDeck
    {
        #region Cards
        private List<AristocratCard> AristocratCards;
        private List<DevelopmentCard> DevelopmentCardsDeck;
        #endregion
        #region Markers
        private Dictionary<MarkerColor, int> Markers;
        private MarkerColor JokerColor;
        #endregion

        Random Rnd = new Random();

        public GameDeck()
        {
            InitializeContainers();
        }

        private void InitializeContainers()
        {
            AristocratCards = new List<AristocratCard>();
            DevelopmentCardsDeck = new List<DevelopmentCard>();
            Markers = new Dictionary<MarkerColor, int>();
        }

        public void ParseFullGameStateUpdate(FullGameStateUpdateMessage fullGameUpdateMessage)
        {
            InitializeContainers();

            JokerColor = EnumUtils.GetEnumValueFromName<MarkerColor>(fullGameUpdateMessage.Joker);

            foreach(AristocratCardEntity ace in fullGameUpdateMessage.AristocratCards)
            {
                AristocratCards.Add(ace.Parse());
            }

            foreach (DevelopmentCardEntity dce in fullGameUpdateMessage.DevelopmentCards)
            {
                DevelopmentCardsDeck.Add(dce.Parse());
            }

            foreach(MarkerEntity me in fullGameUpdateMessage.Markers)
            {
                Markers.Add(EnumUtils.GetEnumValueFromName<MarkerColor>(me.Name), me.Count);
            }
        }

        public List<DevelopmentCard> GetDevelopmentCards()
        {
            return DevelopmentCardsDeck;
        }

        public bool AreMarkersForOneKindAvailable()
        {
            foreach(KeyValuePair<MarkerColor, int> marker in Markers)
            {
                if (marker.Value >= 4 && marker.Key != JokerColor) return true;
            }

            return false;
        }

        public bool IsAnyMarkerAvailable()
        {
            foreach (KeyValuePair<MarkerColor, int> marker in Markers)
            {
                if (marker.Value >= 1 && marker.Key != JokerColor) return true;
            }

            return false;
        }

        public DevelopmentCard GetRandomCardToReserve()
        {
            return DevelopmentCardsDeck[Rnd.Next(DevelopmentCardsDeck.Count)];
        }

        public AristocratCard GetRandomAristocratCardToChase()
        {
            return AristocratCards[Rnd.Next(AristocratCards.Count)];
        }

        public MarkerColor GetRandomOneKindMarkerToTake()
        {
            List<MarkerColor> oneKindMarkersAvailable = new List<MarkerColor>();

            foreach (KeyValuePair<MarkerColor, int> marker in Markers)
            {
                if (marker.Value >= 4 && marker.Key != JokerColor) oneKindMarkersAvailable.Add(marker.Key);
            }

            return oneKindMarkersAvailable[Rnd.Next(oneKindMarkersAvailable.Count)];
        }

        public void RemoveDevelopmentCard(DevelopmentCard developmentCard)
        {
            DevelopmentCardsDeck.Remove(developmentCard);
        }

        public List<MarkerColor> GetRandomMultipleKindMarkersToTake()
        {
            List<MarkerColor> multipleKindMarkersAvailable = new List<MarkerColor>();
            List<MarkerColor> markersToTake = new List<MarkerColor>();

            foreach (KeyValuePair<MarkerColor, int> marker in Markers)
            {
                if (marker.Value >= 1 && marker.Key != JokerColor) multipleKindMarkersAvailable.Add(marker.Key);
            }

            if(multipleKindMarkersAvailable.Count <= 3)
            {
                markersToTake.AddRange(multipleKindMarkersAvailable);
            }
            else
            {
                for(int i = 0; i < 3; i++)
                {
                    MarkerColor markerToTake = multipleKindMarkersAvailable[Rnd.Next(multipleKindMarkersAvailable.Count)];
                    markersToTake.Add(markerToTake);
                    multipleKindMarkersAvailable.Remove(markerToTake);
                }
            }

            return markersToTake;
        }
    }
}
