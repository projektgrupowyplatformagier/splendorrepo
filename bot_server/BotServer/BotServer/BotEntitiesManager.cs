﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BotServer
{
    public static class BotEntitiesManager
    {
        private static int MAX_BOTS = 10;
        private static List<BotEntity> AliveBots = new List<BotEntity>();

        public static bool RequestNewBot(string gameRoomAddress)
        {
            if (AliveBots.Count == MAX_BOTS)
                return false;

            BotEntity newBot = new BotEntity(gameRoomAddress);

            newBot.Start();

            AliveBots.Add(newBot);

            return true;
        }

        public static void HandleBotDeath(BotEntity botEntity)
        {
            AliveBots.Remove(botEntity);
            Console.WriteLine("Bot died");
        }
    }
}
