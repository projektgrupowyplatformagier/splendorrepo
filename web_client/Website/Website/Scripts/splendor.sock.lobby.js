var ws; 
var connected = false;

function selectGameRoom(e) {
    if(gameRoomSelected) return;
    
    var clickTarget = e.target;
    
    var gameRoomAlias = clickTarget.parentElement.id.replace('gameroom-', '');
    
    gameRoomSelected = true;

    ws.close();
    
    connectToGameRoom(gameRoomAlias);
    
    $('#chatView').hide();
    $('#gameRoomView').show();
}

function lobbyHandleChatMessage(receivedData) {
    if(receivedData.SenderType == 0) {
        var txt = $('#chatTextArea');
        txt.val(txt.val() + receivedData.Message + '\n'); 
    } else if(receivedData.SenderType == 1) {
        var txt = $('#chatTextArea');
        txt.val(txt.val() + receivedData.Sender + ": " + receivedData.Message + '\n'); 
    } 
}

function lobbyHandleRoomsList(receivedData) {
    if(receivedData.GameRooms != null && receivedData.GameRooms != undefined) {
        if(receivedData.GameRooms.length > 0) {
            $('#gamerooms > ul').empty();
            
            for(var gameRoomIdx in receivedData.GameRooms) {
                var gameRoom = receivedData.GameRooms[gameRoomIdx];
                
                if (gameRoom.GameRoomPlayersPresent < gameRoom.GameRoomPlayersMax)
                    $('#gamerooms > ul').append('<li id="gameroom-' + gameRoom.GameRoomAlias + '"><a href="#">' + gameRoom.GameRoomName + '</a> (' + gameRoom.GameRoomPlayersPresent + '/' + gameRoom.GameRoomPlayersMax + ')</li>');
                else
                    $('#gamerooms > ul').append('<li id="gameroom-' + gameRoom.GameRoomAlias + '">' + gameRoom.GameRoomName + ' (' + gameRoom.GameRoomPlayersPresent + '/' + gameRoom.GameRoomPlayersMax + ')</li>');
                
                $('#gamerooms > ul > li > a').click(selectGameRoom);
            }
        }
    }
}

function lobbyHandlePlayersList(receivedData) {
    if(receivedData.Players != null && receivedData.Players != undefined) {
        if(receivedData.Players.length > 0) {
            $('#present > ul').empty();
            
            for(var playerIdx in receivedData.Players) {
                var player = receivedData.Players[playerIdx];
                $('#present > ul').append('<li>' + player + '</li>');
            }
        }
    }
}

function lobbyHandleGoToGameRoom(receivedData) {
    if (receivedData.GameRoom != null && receivedData.GameRoom != undefined) {
        ws.close();

        connectToGameRoom(receivedData.GameRoom);

        $('#chatView').hide();
        $('#gameRoomView').show();
    }
}

var lobbyMessageHandlers =
[
    //Chat message
    lobbyHandleChatMessage,
    //Rooms list
    lobbyHandleRoomsList,
    //Players list
    lobbyHandlePlayersList,
    // Game start vote request
    null,
    // Game start
    null,
    // Turn notification
    null,
    // Game state update
    null,
    // GameStart vote failure
    null,
    // Request bot - Server only
    null,
    // Go-to GameRoom message
    lobbyHandleGoToGameRoom
]

function lobbyMessageHandler(evt) {
    var receivedData = JSON.parse(evt.data);
    
    if(lobbyMessageHandlers[receivedData.Type] != undefined && lobbyMessageHandlers[receivedData.Type] != null) 
        lobbyMessageHandlers[receivedData.Type](receivedData);
}

function connect() {
    if(connected) return;
    
    ws = new WebSocket("ws://127.0.0.1:4649/Lobby?uid=" + userId);
    ws.onopen = function () {
        connected = true;
        
        var playersOnlineRequest = {
            Type: "PLAYERS_ONLINE_REQUEST"
        };
        ws.send(JSON.stringify(playersOnlineRequest));
        
        var gameRoomsRequest = {
            Type: "GAME_ROOMS_REQUEST"
        };
        ws.send(JSON.stringify(gameRoomsRequest));
    };

    ws.onmessage = lobbyMessageHandler;

    ws.onclose = function () {
        connected = false;
    };
};