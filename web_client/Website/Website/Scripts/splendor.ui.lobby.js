﻿$('#chatForm').submit(function(e) {
    e.preventDefault();    

    if(connected) {
        var chatMessage = {
            Type: "CHAT",
            Message: $('#chatMessage').val()
        };
        ws.send(JSON.stringify(chatMessage));
        
        $('#chatMessage').val('');
    } else {
        alert("You have to be connected to the server!");
    }
});

$('#createGameRoom').submit(function(e) {
    e.preventDefault();   
    
    if(connected) {
        var createGameRoomRequest = {
            Type: "CREATE_GAME_ROOM_REQUEST"
        };
        ws.send(JSON.stringify(createGameRoomRequest));
    } else {
        alert("You have to be connected to the server!");
    }
});