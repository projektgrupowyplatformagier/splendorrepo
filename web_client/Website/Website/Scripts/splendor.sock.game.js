﻿var gameRoomWs;
var gameRoomConnected = false;

var gameRoomSelected = false;

var GameData = {
    Joker: "",
    DevelopmentCards: [[], [], []],
    AristocratCards: [],
    Markers: {}
}

var PlayerData = {
    DevelopmentCards: [],
    ReservedCards: [],
    AristocratCards: [],
    Markers: {}
}

var TurnData = {
    ReservedMarkers: {},
    ReservedCards: [],
    BoughtCards: []
}

function clearTurnData() {
    TurnData = {
        ReservedMarkers: {},
        ReservedCards: [],
        BoughtCards: []
    }
}

function gameHandleChatMessage(receivedData) {
    if(receivedData.SenderType == 0) {
        var txt = $('#game-chatTextArea');
        txt.val(txt.val() + receivedData.Message + '\n'); 
    } else if(receivedData.SenderType == 1) {
        var txt = $('#game-chatTextArea');
        txt.val(txt.val() + receivedData.Sender + ": " + receivedData.Message + '\n'); 
    } 
}

function gameHandlePlayersList(receivedData) {
    if(receivedData.Players != null && receivedData.Players != undefined) {
        if(receivedData.Players.length > 0) {
            $('#game-present > ul').empty();
            
            for(var playerIdx in receivedData.Players) {
                var player = receivedData.Players[playerIdx];
                $('#game-present > ul').append('<li>' + player + '</li>');
            }
        }
    }
}

function gameHandleGameStartVoteRequest(receivedData) {
    if(receivedData.Requester != null && receivedData.Requester != undefined) {
        var vote;
        if (confirm('Gracz ' + receivedData.Requester + ' chce rozpocząć grę. Czy zgadzasz się na rozpoczęcie gry?')) {
            // Send yes
            vote = true;
        } else {
            // Send no
            vote = false;
        }
        
        var gameStartVote = {
            Type: "GAME_START_VOTE",
            Vote: vote
        };
        gameRoomWs.send(JSON.stringify(gameStartVote));
    }
}

function gameHandleGameStateFullUpdate(receivedData) {
    /**
     ReceivedDataStructure
     
     UpdateReason
     Joker
     AristocratCards - Array
     - 0:
     -- Id
     -- Prestige
     -- RequiredBonuses - Array
     --- 0:
     ---- Count
     ---- Name
     DevelopmentCards - Array
     - 0:
     -- ConsumedMarkers - Array
     --- 0:
     ---- Count
     ---- Name
     -- Id
     -- Tier
     -- Prestige
     -- ProducedBonus
     Markers - Array
     - 0:
     -- Count
     -- Name
     
     */
    if(receivedData.UpdateReason == "GAME_START") {
        $('#requestBot').find('input[type="submit"]').prop('disabled', true);
    }
    
    PlayerData.Markers = {};
    for(var marker in receivedData.PlayerInventory.Markers) {
        PlayerData.Markers[receivedData.PlayerInventory.Markers[marker].Name] = receivedData.PlayerInventory.Markers[marker].Count;
    }
    
    PlayerData.DevelopmentCards = [];
    for(var devCard in receivedData.PlayerInventory.DevelopmentCards) {
        var newDevCard = receivedData.PlayerInventory.DevelopmentCards[devCard];
        
        PlayerData.DevelopmentCards.push(newDevCard);
    }
    
    PlayerData.ReservedCards = [];
    for(var devCard in receivedData.PlayerInventory.ReservedDevelopmentCards) {
        var newDevCard = receivedData.PlayerInventory.ReservedDevelopmentCards[devCard];
        
        PlayerData.ReservedCards.push(newDevCard);
    }
    
    PlayerData.AristocratCards = receivedData.PlayerInventory.AristocratCards;
     
    //TODO: Add validation
    GameData.Joker = receivedData.Joker;
    GameData.AristocratCards = receivedData.AristocratCards;
    
    //Dev Cards and Markers are to be treated a little different
    GameData.Markers = {};
    for(var marker in receivedData.Markers) {
        GameData.Markers[receivedData.Markers[marker].Name] = receivedData.Markers[marker].Count;
    }
    
    GameData.DevelopmentCards = [[], [], []];
    for(var devCard in receivedData.DevelopmentCards) {
        var newDevCard = receivedData.DevelopmentCards[devCard];
        
        GameData.DevelopmentCards[newDevCard.Tier].push(newDevCard);
    }
    
    setUpInterface();
}

function gameHandleTurnNotification(receivedData) {
    prepareTurnInterface();
}

function gameHandleGameStateUpdate(receivedData) {
    
}

function gameHandleGameStartVoteFailure(receivedData) {
    reenableVoteButton();
}

function gameHandleGoToGameRoom(receivedData) {
    if (receivedData.GameRoom != null && receivedData.GameRoom != undefined) {
        ws.close();

        connectToGameRoom(receivedData.GameRoom);

        $('#chatView').hide();
        $('#gameRoomView').show();
    }
}

var gameMessageHandlers =
[
    // Chat message
    gameHandleChatMessage,
    // Rooms list - Lobby only
    null,
    // Players list
    gameHandlePlayersList,
    // Game start vote request
    gameHandleGameStartVoteRequest,
    // Game start
    gameHandleGameStateFullUpdate,
    // Turn notification
    gameHandleTurnNotification,
    // Game state update
    gameHandleGameStateUpdate,
    // GameStart vote failure
    gameHandleGameStartVoteFailure,
    // Request bot - Server only
    null,
    // Go-to GameRoom message
    gameHandleGoToGameRoom
]

function gameMessageHandler(evt) {
    var receivedData = JSON.parse(evt.data);
    
    if(gameMessageHandlers[receivedData.Type] != undefined && gameMessageHandlers[receivedData.Type] != null) 
        gameMessageHandlers[receivedData.Type](receivedData);
}

function connectToGameRoom(gameRoomAlias) {
    if(gameRoomConnected) return;
    
    gameRoomWs = new WebSocket("ws://127.0.0.1:4649/" + gameRoomAlias + "?uid=" + userId);
    gameRoomWs.onopen = function () {
        gameRoomConnected = true;
        
        var playersOnlineRequest = {
            Type: "PLAYERS_ONLINE_REQUEST"
        };
        gameRoomWs.send(JSON.stringify(playersOnlineRequest));
    };

    gameRoomWs.onmessage = gameMessageHandler;
    gameRoomWs.onclose = function () {
        // websocket is closed.
        //alert("Connection is closed...");
        gameRoomConnected = false;
    };
};