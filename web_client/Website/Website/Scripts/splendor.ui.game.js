﻿$('#game-chatForm').submit(function(e) {
    e.preventDefault();    

    if(gameRoomConnected) {
        var chatMessage = {
            Type: "CHAT",
            Message: $('#game-chatMessage').val()
        };
        gameRoomWs.send(JSON.stringify(chatMessage));
        
        $('#game-chatMessage').val('');
    } else {
        alert("You have to be connected to the server!");
    }
});

$('#requestGameStartVote').submit(function(e) {
    e.preventDefault();   
    
    if(gameRoomConnected) {
        var createGameRoomRequest = {
            Type: "GAME_START_VOTE_REQUEST"
        };
        gameRoomWs.send(JSON.stringify(createGameRoomRequest));

        $(this).find('input[type="submit"]').prop('disabled', true);
        $('#requestBot').find('input[type="submit"]').prop('disabled', true);
    } else {
        alert("You have to be connected to the server!");
    }
});

$('#requestBot').submit(function(e) {
    e.preventDefault();   
    
    if(gameRoomConnected) {
        var requestBotRequest = {
            Type: "REQUEST_BOT_REQUEST"
        };
        gameRoomWs.send(JSON.stringify(requestBotRequest));

        $(this).find('input[type="submit"]').prop('disabled', true);
    } else {
        alert("You have to be connected to the server!");
    }
});

function getMarkersDiv(markers) {
    var outDiv = '<ul>';
    
    for(var id in markers) {
        outDiv += '<li>' + markers[id].Name + ': ' + markers[id].Count + '</li>';
    }
    
    outDiv += '</ul>';
    
    return outDiv;
}

function getDevCardDiv(devCard, hasReserveButton, hasBuyButton) {
    var retDiv = '<div id="devCard-' + devCard.Tier + '-' + devCard.Id + '" style="border: 1px solid black;">' +
        '<ul>' +
            '<li>Poziom: ' + devCard.Tier + '</li>' +
            '<li>Id: ' + devCard.Id + '</li>' +
            '<li>Prestiż: ' + devCard.Prestige + '</li>' +
            '<li>Produkowany bonus: ' + devCard.ProducedBonus + '</li>' +
            '<li>Potrzebne znaczniki<br />' +
            getMarkersDiv(devCard.ConsumedMarkers) +
            '</li>' +
        '</ul><br />';
    if(hasReserveButton || hasBuyButton) {
        retDiv += '<form>';
        if(hasBuyButton) retDiv += '<button class="buy-button" type="button" disabled>Kup kartę</button>';
        if(hasReserveButton && hasBuyButton) retDiv += '<br />';
        if(hasReserveButton) retDiv += '<button class="reserve-button" type="button" disabled>Zarezerwuj kartę</button>';
        retDiv += '</form>';
    }
    
    retDiv += '</div>';
    
    return retDiv;
}

function addDevCard(tier, devCard) {
    $('#devCards' + tier).append(getDevCardDiv(devCard, true, true));
}

function addPlayerDevCard(devCard) {
    $('#myDevCards').append(getDevCardDiv(devCard, false, false));
}

function addPlayerReservedDevCard(devCard) {
    $('#myReservedDevCards').append(getDevCardDiv(devCard, false, true));
}

function getAristocratCardDiv(aristocratCard) {
    return '<div id="aristocratCard-' + aristocratCard.Id + '" style="border: 1px solid black;">' +
        '<ul>' +
            '<li>Id: ' + aristocratCard.Id + '</li>' +
            '<li>Prestiż: ' + aristocratCard.Prestige + '</li>' +
            '<li>Potrzebne bonusy<br />' +
            getMarkersDiv(aristocratCard.RequiredBonuses) +
            '</li>' +
        '</ul>' +        
    '</div>';
}

function addAristocratCard(aristocratCard) {
    $('#aristocratCards').append(getAristocratCardDiv(aristocratCard));
}

function addPlayerAristocratCard(aristocratCard) {
    $('#myAristocratCards').append(getAristocratCardDiv(aristocratCard));
}

function addMarker(name, count) {
    if(GameData.Joker == name)
        $('#markers > ul').append('<li id="deckMarker-' + name + '"><form><button type="button" class="get-marker" disabled><i><b>' + name + '</b></i>: ' + count + '</button></form></li>');
    else
        $('#markers > ul').append('<li id="deckMarker-' + name + '"><form><button type="button" class="get-marker" disabled><b>' + name + '</b>: ' + count + '</button></form></li>');
}

function enableMarker(name) {
    $('#deckMarker-' + name + ' > form > button.get-marker').prop('disabled', false);
    $('#deckMarker-' + name + ' > form > button.get-marker').click(getMarker);
}

function addInvMarker(name, count) {
    if(GameData.Joker == name)
        $('#invMarkers > ul').append('<li id="invMarker-' + name + '"><i><b>' + name + '</b></i>: ' + count + '</li>');
    else
        $('#invMarkers > ul').append('<li id="invMarker-' + name + '"><b>' + name + '</b>: ' + count + '</li>');
}

function resetGameBoardContainer() {
    $('#game-gameboard-containter').empty();
    $('#game-gameboard-containter').html('<div id="inventories"><b>Twoja ręka</b><br /><div id="invMarkers" style="float: left">Znaczniki<ul id="deckMarkersList"></ul></div><br /><div id="myDevCards" style="float: left"></div><br /><div id="myReservedDevCards" style="float: left"></div><br /><div id="myAristocratCards" style="float: left"></div></div><br /><div id="gameDeck"><div id="aristocratCards" style="float: left">Arystokraci</div><div id="devCards0" style="float: left">Karty rozwoju - Poziom 1</div><div id="devCards1" style="float: left">Karty rozwoju - Poziom 2</div><div id="devCards2" style="float: left">Karty rozwoju - Poziom 3</div><div id="markers" style="float: left">Znaczniki<ul id="deckMarkersList"></ul></div></div>');  
}

function setUpInterface() {
    resetGameBoardContainer();
    $('#game-gameboard-containter').show();
    
    for(var tier in GameData.DevelopmentCards) {
        for(var devCard in GameData.DevelopmentCards[tier]) {
            addDevCard(tier, GameData.DevelopmentCards[tier][devCard]);
        }
    }
    
    for(var aristocrat in GameData.AristocratCards) {
        addAristocratCard(GameData.AristocratCards[aristocrat]);
    }
    
    for(var marker in GameData.Markers) {
        addMarker(marker, GameData.Markers[marker]);
        addInvMarker(marker, PlayerData.Markers[marker]);
    }
    
    for(var aristocrat in PlayerData.AristocratCards) {
        addPlayerAristocratCard(PlayerData.AristocratCards[aristocrat]);
    }
    
    for(var devCard in PlayerData.DevelopmentCards) {
        addPlayerDevCard(PlayerData.DevelopmentCards[devCard]);
    }
    
    for(var devCard in PlayerData.ReservedCards) {
        addPlayerReservedDevCard(PlayerData.ReservedCards[devCard]);
    }
}

function enableDevCardIfPossible(tier, devCard) {
    var canReserveCard = true;
    var canBuyCard = true;
    
    if(PlayerData.ReservedCards.length >= 3)
        canReserveCard = false;
        
    for(var id in devCard.ConsumedMarkers) {
        if(devCard.ConsumedMarkers[id].Count > PlayerData.Markers[devCard.ConsumedMarkers[id].Name]) {
            canBuyCard = false;
            break;
        }
    }
        
    changeDevCardButtonsStates(canReserveCard, canBuyCard, devCard, tier);
}

function validateTurnAction(turnActionData) {
    var turnActionValid = false;
    var cardIsOnDeck = false;
    var cardIsReservedByPlayer = false;
    if(turnActionData.Action == "RESERVE_CARD") {
        //To check here:
        //1. Is card available on deck?
        //2. Does player has less than three reserved cards in his hand?
        for(var tier in GameData.DevelopmentCards) {
            for(var cardIdx in GameData.DevelopmentCards[tier]) {
                if(GameData.DevelopmentCards[tier][cardIdx].Id == turnActionData.CardId) {
                    if(tier == turnActionData.CardTier) {
                        cardIsOnDeck = true;
                    }
                    break;
                }
            }
        }
        
        if(cardIsOnDeck) {
            if(PlayerData.ReservedCards.length < 3) {
                turnActionValid = true;
            }
        }
    } else if(turnActionData.Action == "BUY_CARD") {
        //To check here:
        //1. Is card available on deck?
        //2. Can he afford the card?
        var foundCard;
        var canBuyCard = true;
        for(var tier in GameData.DevelopmentCards) {
            for(var cardIdx in GameData.DevelopmentCards[tier]) {
                if(GameData.DevelopmentCards[tier][cardIdx].Id == turnActionData.CardId) {
                    if(tier == turnActionData.CardTier) {
                        foundCard = GameData.DevelopmentCards[tier][cardIdx];
                        cardIsOnDeck = true;
                    }
                    break;
                }
            }
        }
        
        if(cardIsOnDeck) {
            for(var id in foundCard.ConsumedMarkers) {
                if(foundCard.ConsumedMarkers[id].Count > PlayerData.Markers[foundCard.ConsumedMarkers[id].Name]) {
                    canBuyCard = false;
                    break;
                }
            }
            
            if(canBuyCard) {
                turnActionValid = true;
            }
        } else {
            for(var cardIdx in PlayerData.ReservedCards) {
                if(PlayerData.ReservedCards[cardIdx].Id == turnActionData.CardId) {
                    foundCard = PlayerData.ReservedCards[cardIdx];
                    cardIsReservedByPlayer = true;
                    break;
                }
            }
            
            if(cardIsReservedByPlayer) {
                for(var id in foundCard.ConsumedMarkers) {
                    if(foundCard.ConsumedMarkers[id].Count > PlayerData.Markers[foundCard.ConsumedMarkers[id].Name]) {
                        canBuyCard = false;
                        break;
                    }
                }
                
                if(canBuyCard) {
                    turnActionValid = true;
                }
            }
        }
    } else if(turnActionData.Action == "TAKE_MARKERS") {
        //To check here:
        //1. Max 2 markers of same color if markers count >= 4
        //2. Max 3 different markers
        //3. Check that player took 1 or 3 marker types
        var markersTypesCount = getObjectCount(turnActionData.TakenMarkers);
        if(markersTypesCount == 1) {
            turnActionValid = true;
            var takenMarker;
            var takenMarkersCount = 0;
            for(var marker in turnActionData.TakenMarkers) {
                takenMarker = marker;
                takenMarkersCount++; //Just in case
                if(turnActionData.TakenMarkers[marker] != 2) {
                    turnActionValid = false;
                    break;
                }
            }
            
            if(takenMarkersCount != 1) turnActionValid = false;
            
            if(turnActionValid) {
                if(GameData.Markers[takenMarker] + turnActionData.TakenMarkers[takenMarker] < 4) {
                    turnActionValid = false;
                }
            }
        } else if(markersTypesCount == 3) {
            turnActionValid = true;
            for(var marker in turnActionData.TakenMarkers) {
                if(turnActionData.TakenMarkers[marker] != 1) {
                    turnActionValid = false;
                    break;
                }
            }
        }
    }
    
    return turnActionValid;
}

function reserveCard() {
    var devCardId = $(this)[0].parentNode.parentNode.id;
    devCardId = devCardId.replace('devCard-', '');
    
    var devCardTier = devCardId[0];
    devCardId = devCardId.replace(devCardTier + '-', '');
    
    //alert("Trying to reserve card " + devCardId + " of tier " + devCardTier + "!");
    
    if(gameRoomConnected) {
        var turnAction = {
            Type: "TURN_ACTION",
            Action: "RESERVE_CARD",
            CardTier: devCardTier,
            CardId: devCardId
        };
        
        if(validateTurnAction(turnAction)) {
            gameRoomWs.send(JSON.stringify(turnAction));
        } else {
            alert("You cannot do that!");
        }        
    } else {
        alert("You have to be connected to the server!");
    }
}

function buyCard() {
    var devCardId = $(this)[0].parentNode.parentNode.id;
    devCardId = devCardId.replace('devCard-', '');
    
    var devCardTier = devCardId[0];
    devCardId = devCardId.replace(devCardTier + '-', '');
    
    //alert("Trying to reserve card " + devCardId + " of tier " + devCardTier + "!");
    
    if(gameRoomConnected) {
        var turnAction = {
            Type: "TURN_ACTION",
            Action: "BUY_CARD",
            CardTier: devCardTier,
            CardId: devCardId
        };
        
        if(validateTurnAction(turnAction)) {
            gameRoomWs.send(JSON.stringify(turnAction));
        } else {
            alert("You cannot do that!");
            //TODO: Request full game data again
        }        
    } else {
        alert("You have to be connected to the server!");
    }
}

function changeDevCardButtonsStates(reserve, buy, devCard, tier) {
    if(reserve)
    {
        $('#devCard-' + tier + '-' + devCard.Id + ' > form > button.reserve-button').prop('disabled', false);
        $('#devCard-' + tier + '-' + devCard.Id + ' > form > button.reserve-button').click(reserveCard);
    }
    else
    {
        $('#devCard-' + tier + '-' + devCard.Id + ' > form > button.reserve-button').prop('disabled', true);
        $('#devCard-' + tier + '-' + devCard.Id + ' > form > button.reserve-button').click(function() {});
    }
    
    if(buy)
    {
        $('#devCard-' + tier + '-' + devCard.Id + ' > form > button.buy-button').prop('disabled', false);
        $('#devCard-' + tier + '-' + devCard.Id + ' > form > button.buy-button').click(buyCard);
    }
    else
    {
        $('#devCard-' + tier + '-' + devCard.Id + ' > form > button.buy-button').prop('disabled', true);
        $('#devCard-' + tier + '-' + devCard.Id + ' > form > button.buy-button').click(function() {});
    }
}

function disableAllCards() {
    for(var tier in GameData.DevelopmentCards) {
        for(var devCard in GameData.DevelopmentCards[tier]) {
            changeDevCardButtonsStates(false, false, devCard, tier);
        }
    }
}

function prepareTurnInterface() {
    alert("Nadeszła twoja tura!");
    
    clearTurnData();
    
    for(var tier in GameData.DevelopmentCards) {
        for(var devCard in GameData.DevelopmentCards[tier]) {
            enableDevCardIfPossible(tier, GameData.DevelopmentCards[tier][devCard]);
        }
    }
    
    for(var devCard in PlayerData.ReservedCards) {
        enableDevCardIfPossible(PlayerData.ReservedCards[devCard].Tier, PlayerData.ReservedCards[devCard]);
    }
    
    for(var marker in GameData.Markers) {
        if(GameData.Joker == marker) continue;
        if(GameData.Markers[marker] == 0) continue;
        enableMarker(marker);
    }
}

function takeMarker(markerName) {
    if(GameData.Markers[markerName] <= 0) return false;
    
    if(TurnData.ReservedMarkers[markerName] != undefined && TurnData.ReservedMarkers[markerName] != null) {
        for(var marker in GameData.Markers) {
            $('#deckMarker-' + marker + ' > form > button.get-marker').prop('disabled', true);
        }
    } else if(GameData.Markers[markerName] < 4) {
        $('#deckMarker-' + markerName + ' > form > button.get-marker').prop('disabled', true);
    }
    
    GameData.Markers[markerName]--;
    var markerTitle = '<b>' + markerName + '</b>';
    
    if(markerName == GameData.Joker) {
        markerTitle = '<i>' + markerTitle + '</i>';
    }
    
    $('#deckMarker-' + markerName + ' > form > button.get-marker').html(markerTitle + ': ' + GameData.Markers[markerName]);
    
    return true;
}

function getMarker() {
    var markerName = $(this)[0].parentNode.parentNode.id;
    markerName = markerName.replace('deckMarker-', '');
    var concludeTurn = false;
    
    if(!takeMarker(markerName)) return;
    
    if(TurnData.ReservedMarkers.length == 0) {
        disableAllCards();
    }
    
    if(TurnData.ReservedMarkers[markerName] != undefined && TurnData.ReservedMarkers[markerName] != null) {
        TurnData.ReservedMarkers[markerName]++;
        
        concludeTurn = true;
    } else {
        TurnData.ReservedMarkers[markerName] = 1;
    }
    
    if(getObjectCount(TurnData.ReservedMarkers) == 3) {
        concludeTurn = true;
    }
    
    if(concludeTurn) {
        //Send data
        if(gameRoomConnected) {
            var turnAction = {
                Type: "TURN_ACTION",
                Action: "TAKE_MARKERS",
                TakenMarkers: TurnData.ReservedMarkers 
            };
            
            if(validateTurnAction(turnAction)) {
                gameRoomWs.send(JSON.stringify(turnAction));
            } else {
                alert("You cannot do that!");
                //TODO: Request full game data again
            }        
        } else {
            alert("You have to be connected to the server!");
        }
    }
}

function reenableVoteButton() {
    $('#requestGameStartVote').find('input[type="submit"]').prop('disabled', false);
    $('#requestBot').find('input[type="submit"]').prop('disabled', false);
}