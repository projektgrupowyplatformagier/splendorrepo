﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainServerApp.Communication
{
    class GameStartVoteFailureMessage : Message
    {
        public GameStartVoteFailureMessage()
        {
            Type = EMessageType.GAME_START_VOTE_FAILURE;
        }
    }
}
