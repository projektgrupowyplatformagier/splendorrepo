﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainServerApp.Communication
{
    class GameStartVoteRequest : Message
    {
        public string Requester;

        public GameStartVoteRequest(string requester)
        {
            Requester = requester;
            Type = EMessageType.GAME_START_VOTE_REQUEST;
        }
    }
}
