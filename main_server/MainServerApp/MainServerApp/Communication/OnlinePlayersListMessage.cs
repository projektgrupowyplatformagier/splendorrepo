﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainServerApp.Communication
{
    class OnlinePlayersListMessage : Message
    {
        public List<string> Players;

        public OnlinePlayersListMessage()
        {
            Type = EMessageType.PLAYERS_ONLINE_LIST;
            Players = new List<string>();
        }

        public void AddPlayer(string nickname)
        {
            Players.Add(nickname);
        }
    }
}
