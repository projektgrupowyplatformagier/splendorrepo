﻿using MainServerApp.Communication.DataTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainServerApp.Communication
{
    class GameRoomsListMessage : Message
    {
        public List<GameRoomEntity> GameRooms;

        public GameRoomsListMessage()
        {
            Type = EMessageType.GAME_ROOMS_LIST;
            GameRooms = new List<GameRoomEntity>();
        }

        public void AddGameRoomEntity(GameRoomEntity entity)
        {
            GameRooms.Add(entity);
        }
    }
}
