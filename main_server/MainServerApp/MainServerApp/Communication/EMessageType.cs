﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainServerApp.Communication
{
    enum EMessageType
    {
        CHAT,
        GAME_ROOMS_LIST,
        PLAYERS_ONLINE_LIST,
        GAME_START_VOTE_REQUEST,
        FULL_GAME_STATE_UPDATE,
        TURN_NOTIFICATION,
        GAME_STATE_UPDATE,
        GAME_START_VOTE_FAILURE,
        REQUEST_BOT_REQUEST,
        GO_TO_GAME_ROOM
    }
}
