﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainServerApp.Communication
{
    class GoToGameRoomMessage : Message
    {
        public string GameRoom;

        public GoToGameRoomMessage(string gameRoom)
        {
            GameRoom = gameRoom;
            Type = EMessageType.GO_TO_GAME_ROOM;
        }
    }
}
