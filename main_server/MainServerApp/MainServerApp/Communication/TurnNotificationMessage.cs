﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainServerApp.Communication
{
    class TurnNotificationMessage : Message
    {
        public TurnNotificationMessage()
        {
            Type = EMessageType.TURN_NOTIFICATION;
        }
    }
}
