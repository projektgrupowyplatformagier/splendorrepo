﻿using MainServerApp.GameItems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace MainServerApp.Communication.DataTypes
{
    class PlayerInventoryEntity
    {
        public string Name;
        public List<AristocratCardEntity> AristocratCards;
        public List<DevelopmentCardEntity> DevelopmentCards;
        public List<DevelopmentCardEntity> ReservedDevelopmentCards;
        public List<MarkerEntity> Markers;

        public PlayerInventoryEntity(string name, List<AristocratCard> aristocratCards, Dictionary<MarkerColor, int> markers, List<DevelopmentCard> developmentCards, List<DevelopmentCard> reservedDevelopmentCards)
        {
            Name = name;
            AristocratCards = new List<AristocratCardEntity>();
            DevelopmentCards = new List<DevelopmentCardEntity>();
            ReservedDevelopmentCards = new List<DevelopmentCardEntity>();
            Markers = new List<MarkerEntity>();

            foreach (AristocratCard card in aristocratCards)
            {
                AristocratCards.Add(card.GetEntity());
            }

            foreach (DevelopmentCard card in developmentCards)
            {
                DevelopmentCards.Add(card.GetEntity());
            }

            foreach (DevelopmentCard card in reservedDevelopmentCards)
            {
                ReservedDevelopmentCards.Add(card.GetEntity());
            }

            foreach (KeyValuePair<MarkerColor, int> marker in markers)
            {
                Markers.Add(new MarkerEntity(marker.Key.ToString(), marker.Value));
            }
        }

        public PlayerInventoryEntity(JToken token)
        {
            Name = (string)token["Name"];

            AristocratCards = new List<AristocratCardEntity>();
            DevelopmentCards = new List<DevelopmentCardEntity>();
            ReservedDevelopmentCards = new List<DevelopmentCardEntity>();
            Markers = new List<MarkerEntity>();

            foreach (JToken card in (JArray)token["AristocratCards"])
            {
                AristocratCards.Add(new AristocratCardEntity(card));
            }

            foreach (JToken card in (JArray)token["DevelopmentCards"])
            {
                DevelopmentCards.Add(new DevelopmentCardEntity(card));
            }

            foreach (JToken card in (JArray)token["ReservedDevelopmentCards"])
            {
                ReservedDevelopmentCards.Add(new DevelopmentCardEntity(card));
            }

            foreach (JToken marker in (JArray)token["Markers"])
            {
                Markers.Add(new MarkerEntity(marker));
            }
        }
    }
}
