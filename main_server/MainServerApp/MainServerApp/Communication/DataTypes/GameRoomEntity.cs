﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainServerApp.Communication.DataTypes
{
    class GameRoomEntity
    {
        public string GameRoomName;
        public string GameRoomAlias;
        public int    GameRoomPlayersPresent;
        public int    GameRoomPlayersMax;

        public GameRoomEntity(string name, string alias, int players, int playersMax)
        {
            GameRoomName = name;
            GameRoomAlias = alias;
            GameRoomPlayersPresent = players;
            GameRoomPlayersMax = playersMax;
        }
    }
}
