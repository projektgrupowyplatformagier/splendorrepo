﻿using MainServerApp.GameItems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace MainServerApp.Communication.DataTypes
{
    class AristocratCardEntity
    {
        public string Id;
        public int Prestige;
        public List<MarkerEntity> RequiredBonuses;

        public AristocratCardEntity(JToken card)
        {
            Id = (string)card["Id"];
            Prestige = (int)card["Prestige"];
            RequiredBonuses = new List<MarkerEntity>();

            foreach(JToken marker in (JArray)card["RequiredBonuses"])
            {
                RequiredBonuses.Add(new MarkerEntity(marker));
            }
        }

        public AristocratCardEntity(string id, int prestige)
        {
            Id = id;
            Prestige = prestige;
            RequiredBonuses = new List<MarkerEntity>();
        }

        public void AddRequiredBonus(MarkerColor color, int quantity)
        {
            RequiredBonuses.Add(new MarkerEntity(color.ToString(), quantity));
        }

        public AristocratCard Parse()
        {
            AristocratCard ac = new AristocratCard(Id, AristoCratCardBackground.UNKNOWN.ToString(), Prestige); //TODO: Handle this unknown

            foreach(MarkerEntity me in RequiredBonuses)
            {
                ac.AddRequiredBonus(me.Name, me.Count);
            }

            return ac;
        }
    }
}
