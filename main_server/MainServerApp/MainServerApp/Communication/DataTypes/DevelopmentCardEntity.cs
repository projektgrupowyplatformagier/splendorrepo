﻿using MainServerApp.GameItems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace MainServerApp.Communication.DataTypes
{
    class DevelopmentCardEntity
    {
        public int Tier;
        public string Id;
        public int Prestige;
        public string ProducedBonus;
        public List<MarkerEntity> ConsumedMarkers;

        public DevelopmentCardEntity(DevelopmentCardTier tier, string id, int prestige, MarkerColor producedBonus)
        {
            Tier = (int)tier;
            Id = id;
            Prestige = prestige;
            ProducedBonus = producedBonus.ToString();
            ConsumedMarkers = new List<MarkerEntity>();
        }

        public DevelopmentCardEntity(JToken card)
        {
            Tier = (int)card["Tier"];
            Id = (string)card["Id"];
            Prestige = (int)card["Prestige"];
            ProducedBonus = (string)card["ProducedBonus"];
            ConsumedMarkers = new List<MarkerEntity>();

            foreach (JToken marker in (JArray)card["ConsumedMarkers"])
            {
                ConsumedMarkers.Add(new MarkerEntity(marker));
            }
        }

        public void AddConsumedMarker(MarkerColor color, int quantity)
        {
            ConsumedMarkers.Add(new MarkerEntity(color.ToString(), quantity));
        }

        public DevelopmentCard Parse()
        {
            DevelopmentCard de = new DevelopmentCard((DevelopmentCardTier)Tier, Id, DevelopmentCardBackground.UNKNOWN.ToString(), Prestige, ProducedBonus);

            foreach(MarkerEntity me in ConsumedMarkers)
            {
                de.AddConsumedMarker(me.Name, me.Count);
            }

            return de;
        }
    }
}
