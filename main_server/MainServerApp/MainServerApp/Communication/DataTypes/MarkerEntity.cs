﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace MainServerApp.Communication.DataTypes
{
    class MarkerEntity
    {
        public string Name;
        public int Count;

        public MarkerEntity(string name, int count)
        {
            Name = name;
            Count = count;
        }

        public MarkerEntity(JToken marker)
        {
            Name = (string)marker["Name"];
            Count = (int)marker["Count"];
        }
    }
}
