﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainServerApp.Communication
{
    class ChatMessage : Message
    {
        public string Message;
        public string Sender;
        public ChatSenderType SenderType;

        public ChatMessage(string message, ChatSenderType senderType, string sender)
        {
            Message = message;
            Sender = sender;
            SenderType = senderType;
            Type = EMessageType.CHAT;
        }
    }
}
