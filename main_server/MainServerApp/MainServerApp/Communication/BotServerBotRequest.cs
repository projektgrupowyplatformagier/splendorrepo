﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainServerApp.Communication
{
    class BotServerBotRequest : Message
    {
        public string TargetGameRoom;

        public BotServerBotRequest(string target)
        {
            TargetGameRoom = target;
            Type = EMessageType.REQUEST_BOT_REQUEST;
        }
    }
}
