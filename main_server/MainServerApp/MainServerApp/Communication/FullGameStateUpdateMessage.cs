﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MainServerApp.GameItems;
using MainServerApp.Communication.DataTypes;
using Newtonsoft.Json.Linq;

namespace MainServerApp.Communication
{
    class FullGameStateUpdateMessage : Message
    {
        public string UpdateReason;
        public List<AristocratCardEntity> AristocratCards;
        public List<DevelopmentCardEntity> DevelopmentCards;
        public List<MarkerEntity> Markers;
        public PlayerInventoryEntity PlayerInventory;
        public List<PlayerInventoryEntity> OtherPlayersInventories;

        public string Joker;

        public FullGameStateUpdateMessage(EFullGameStateUpdateReason reason, List<AristocratCard> aristocratCards, Dictionary<MarkerColor, int> markers, List<DevelopmentCard> developmentCardsDeck, MarkerColor jokerColor, PlayerInventoryEntity playerInventory, List<PlayerInventoryEntity> otherPlayersInventories)
        {
            Type = EMessageType.FULL_GAME_STATE_UPDATE;
            UpdateReason = reason.ToString();
            AristocratCards = new List<AristocratCardEntity>();
            DevelopmentCards = new List<DevelopmentCardEntity>();
            Markers = new List<MarkerEntity>();
            Joker = jokerColor.ToString();
            PlayerInventory = playerInventory;
            OtherPlayersInventories = otherPlayersInventories;

            foreach(AristocratCard card in aristocratCards)
            {
                AristocratCards.Add(card.GetEntity());
            }

            foreach(DevelopmentCard card in developmentCardsDeck)
            {
                DevelopmentCards.Add(card.GetEntity());
            }

            foreach(KeyValuePair<MarkerColor, int> marker in markers)
            {
                Markers.Add(new MarkerEntity(marker.Key.ToString(), marker.Value));
            }
        }

        public FullGameStateUpdateMessage(JToken token)
        {
            Type = EMessageType.FULL_GAME_STATE_UPDATE;
            UpdateReason = (string)token["UpdateReason"];
            Joker = (string)token["Joker"];

            AristocratCards = new List<AristocratCardEntity>();
            DevelopmentCards = new List<DevelopmentCardEntity>();
            Markers = new List<MarkerEntity>();
            
            foreach (JToken card in (JArray)token["AristocratCards"])
            {
                AristocratCards.Add(new AristocratCardEntity(card));
            }

            foreach (JToken card in (JArray)token["DevelopmentCards"])
            {
                DevelopmentCards.Add(new DevelopmentCardEntity(card));
            }

            foreach (JToken marker in (JArray)token["Markers"])
            {
                Markers.Add(new MarkerEntity(marker));
            }

            PlayerInventory = new PlayerInventoryEntity(token["PlayerInventory"]);

            /*foreach (JToken playerInventory in (JArray)token["OtherPlayersInventories"])
            {
                OtherPlayersInventories.Add(new PlayerInventoryEntity(playerInventory));
            }*/
        }
    }
}
