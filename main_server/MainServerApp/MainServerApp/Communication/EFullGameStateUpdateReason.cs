﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainServerApp.Communication
{
    enum EFullGameStateUpdateReason
    {
        GAME_START,
        INVALID_TURN_ACTION_ROLLBACK,
        NEW_TURN // TODO: Add partial update message mechanism
    }
}
