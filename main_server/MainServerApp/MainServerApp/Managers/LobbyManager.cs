﻿using MainServerApp.Communication;
using MainServerApp.WebServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebSocketSharp.Server;

namespace MainServerApp.Managers
{
    class LobbyManager
    {
        private List<string> PeopleInRoom;
        private WebSocketServer ParentWSSv;
        private Dictionary<string, GameRoomManager> GameRooms;
        private uint GameRoomCounter;

        public LobbyManager(WebSocketServer wssv)
        {
            PeopleInRoom = new List<string>();
            GameRooms = new Dictionary<string, GameRoomManager>();
            ParentWSSv = wssv;
            GameRoomCounter = 0;
        }

        public Lobby CreateLobby()
        {
            return new Lobby(null, this);
        }

        public void HandlePersonJoin(string nickname)
        {
            PeopleInRoom.Add(nickname);
        }

        public void HandlePersonLeave(string nickname)
        {
            PeopleInRoom.Remove(nickname);
        }

        public string CreateGameRoom()
        {
            string gameRoomName = "GameRoom #" + GameRoomCounter;
            string gameRoomAlias = "GameRoom" + GameRoomCounter;
            GameRoomManager gmmgr = new GameRoomManager(gameRoomName, gameRoomAlias, GameManager.GetMaxNumberOfPlayers(), this);

            GameRooms.Add(gameRoomAlias, gmmgr);

            ParentWSSv.AddWebSocketService<GameRoom>("/" + gameRoomAlias, gmmgr.CreateGameRoom);

            GameRoomCounter++;

            return gameRoomName;
        }

        public void DestroyGameRoom(string gameRoomAlias)
        {
            GameRooms.Remove(gameRoomAlias);
            ParentWSSv.RemoveWebSocketService("/" + gameRoomAlias);
        }

        public string GetGameRoomsMsg()
        {
            GameRoomsListMessage msg = new GameRoomsListMessage();

            foreach (KeyValuePair<string, GameRoomManager> gameroom in GameRooms)
            {
                if (!gameroom.Value.IsGameInProgress())
                {
                    msg.AddGameRoomEntity(gameroom.Value.GetRoomEntity());
                }
            }

            string message = Newtonsoft.Json.JsonConvert.SerializeObject(msg);

            return message;
        }

        public string GetOnlinePlayersMsg()
        {
            OnlinePlayersListMessage msg = new OnlinePlayersListMessage();

            foreach (string nickname in PeopleInRoom)
            {
                msg.AddPlayer(nickname);
            }

            string message = Newtonsoft.Json.JsonConvert.SerializeObject(msg);

            return message;
        }

        internal bool DoesGameRoomExist(string gameRoomAlias)
        {
            return GameRooms.ContainsKey(gameRoomAlias);
        }

        internal string GetGotoGameRoomMsg(string gameRoomAlias)
        {
            GoToGameRoomMessage msg = new GoToGameRoomMessage(gameRoomAlias);

            string message = Newtonsoft.Json.JsonConvert.SerializeObject(msg);

            return message;
        }
    }
}
