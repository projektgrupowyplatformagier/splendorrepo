﻿using System;
using System.Data.SQLite;
using System.IO;

namespace MainServerApp
{
    public class DatabaseManager
    {
        private static SQLiteConnection DatabaseConnection;

        public static void Initialize()
        {
            ConnectDatabase();
            ClearPlayerGameRoomsAssociations();
            ClearGameRooms();
        }

        private static void ClearPlayerGameRoomsAssociations()
        {
            string query = "DELETE FROM GameRooms";
            SQLiteCommand command = new SQLiteCommand(query, DatabaseConnection);
            command.ExecuteNonQuery();
        }

        private static void ClearGameRooms()
        {
            string query = "DELETE FROM UserGameRoomAssociation";
            SQLiteCommand command = new SQLiteCommand(query, DatabaseConnection);
            command.ExecuteNonQuery();
        }

        private static void ConnectDatabase()
        {
            DatabaseConnection = new SQLiteConnection("Data Source=C:/db/mydatabase.sqlite;Version=3;");
            DatabaseConnection.Open();
        }

        public static string GetUsernameFromId(string id)
        {
            string getUserQuery = "SELECT * FROM AspNetUsers WHERE Id = '" + id + "';";
            SQLiteCommand getUserCommand = new SQLiteCommand(getUserQuery, DatabaseConnection);
            SQLiteDataReader getUserResult = getUserCommand.ExecuteReader();

            if(getUserResult.Read())
            {
                return (string)getUserResult["Email"];
            }
            else
            {
                return null;
            }
        }

        internal static void AddGameRoom(string alias)
        {
            string addGameRoomQuery = "INSERT INTO GameRooms (name) VALUES('" + alias + "'); ";
            SQLiteCommand addGameRoomCommand = new SQLiteCommand(addGameRoomQuery, DatabaseConnection);
            addGameRoomCommand.ExecuteNonQuery();
        }

        internal static void AddUserToGameRoom(string userId, string gameRoomAlias)
        {
            string addGameRoomQuery = "INSERT INTO UserGameRoomAssociation (gameroom, userid) VALUES('" + gameRoomAlias + "', '" + userId + "');";
            SQLiteCommand addGameRoomCommand = new SQLiteCommand(addGameRoomQuery, DatabaseConnection);
            addGameRoomCommand.ExecuteNonQuery();
        }

        internal static void DeleteGameRoom(string alias)
        {
            string query0 = "DELETE * FROM UserGameRoomAssociation WHERE gameroom = '" + alias + "'";
            SQLiteCommand command0 = new SQLiteCommand(query0, DatabaseConnection);
            command0.ExecuteNonQuery();

            string query1 = "DELETE * FROM GameRooms WHERE name = '" + alias + "'";
            SQLiteCommand command1 = new SQLiteCommand(query1, DatabaseConnection);
            command1.ExecuteNonQuery();
        }

        internal static bool IsUserInGameRoom(string userId, out string alias)
        {
            string query = "SELECT * FROM UserGameRoomAssociation WHERE userid = '" + userId + "'";
            SQLiteCommand command = new SQLiteCommand(query, DatabaseConnection);
            SQLiteDataReader result = command.ExecuteReader();

            if (result.Read())
            {
                alias = (string)result["gameroom"];
                return true;
            }
            else
            {
                alias = "";
                return false;
            }
        }
    }
}