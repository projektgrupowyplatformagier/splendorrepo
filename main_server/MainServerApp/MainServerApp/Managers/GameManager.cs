﻿using MainServerApp.GameItems;
using MainServerApp.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace MainServerApp.Managers
{
    class GameManager
    {
        // Data from XML
        private static Game GameData;

        // Processed from GameData
        private static List<DevelopmentCard> DevelopmentCardsTier1;
        private static List<DevelopmentCard> DevelopmentCardsTier2;
        private static List<DevelopmentCard> DevelopmentCardsTier3;
        private static List<AristocratCard> AristocratCards;
        private static MarkerColor JokerColor;

        private GameManager()
        {
            
        }

        private static void LoadDevelopmentCards()
        {
            #region Tier 1 Loading
            DevelopmentCardsTier1 = new List<DevelopmentCard>();

            foreach(DevelopmentCardType card in GameData.DevelopmentCards.Level1)
            {
                DevelopmentCard newCard = new DevelopmentCard(DevelopmentCardTier.TIER1, card.Id, card.Background, card.Prestige, card.Produce.Marker);

                foreach(DevelopmentCardTypeMaterial consumedMaterial in card.Consume)
                {
                    newCard.AddConsumedMarker(consumedMaterial.Marker, consumedMaterial.Quantity);
                }

                DevelopmentCardsTier1.Add(newCard);
            }
            #endregion

            #region Tier 2 Loading
            DevelopmentCardsTier2 = new List<DevelopmentCard>();

            foreach (DevelopmentCardType card in GameData.DevelopmentCards.Level2)
            {
                DevelopmentCard newCard = new DevelopmentCard(DevelopmentCardTier.TIER2, card.Id, card.Background, card.Prestige, card.Produce.Marker);

                foreach (DevelopmentCardTypeMaterial consumedMaterial in card.Consume)
                {
                    newCard.AddConsumedMarker(consumedMaterial.Marker, consumedMaterial.Quantity);
                }

                DevelopmentCardsTier2.Add(newCard);
            }
            #endregion

            #region Tier 3 Loading
            DevelopmentCardsTier3 = new List<DevelopmentCard>();

            foreach (DevelopmentCardType card in GameData.DevelopmentCards.Level3)
            {
                DevelopmentCard newCard = new DevelopmentCard(DevelopmentCardTier.TIER3, card.Id, card.Background, card.Prestige, card.Produce.Marker);

                foreach (DevelopmentCardTypeMaterial consumedMaterial in card.Consume)
                {
                    newCard.AddConsumedMarker(consumedMaterial.Marker, consumedMaterial.Quantity);
                }

                DevelopmentCardsTier3.Add(newCard);
            }
            #endregion
        }

        private static void LoadAristocratCards()
        {
            #region Aristocrats Loading
            AristocratCards = new List<AristocratCard>();

            foreach (AristocratType aristocrat in GameData.Aristocrats)
            {
                AristocratCard newAristocrat = new AristocratCard(aristocrat.Id, aristocrat.Background, aristocrat.Prestige);

                foreach (AristocratTypeMaterial requiredMaterial in aristocrat.Reqiure)
                {
                    newAristocrat.AddRequiredBonus(requiredMaterial.Marker, requiredMaterial.Quantity);
                }

                AristocratCards.Add(newAristocrat);
            }
            #endregion
        }

        private static void LoadJokerInfo()
        {
            JokerColor = EnumUtils.GetEnumValueFromName<MarkerColor>(GameData.Markers.Joker.Marker.Id);
        }

        public static void Initialize()
        {
            FileStream fs = new FileStream("..\\..\\GameConfig.xml", FileMode.Open);
            XmlSerializer XmlSerial = new XmlSerializer(typeof(Game));
            XmlReader xr = XmlReader.Create(fs);

            GameData = (Game)XmlSerial.Deserialize(xr);

            LoadDevelopmentCards();
            LoadAristocratCards();
        }

        public static void GetAristocratCards(int playersCount, out List<AristocratCard> cards)
        {
            int aristocratCardsNeeded = GetNumberOfAristocratsForPlayersCount(playersCount);
            cards = new List<AristocratCard>();
            Random rnd = new Random();

            while (aristocratCardsNeeded > 0)
            {
                AristocratCard aristocratCard = AristocratCards[rnd.Next(AristocratCards.Count)];

                while(cards.Contains(aristocratCard))
                {
                    aristocratCard = AristocratCards[rnd.Next(AristocratCards.Count)];
                }

                cards.Add(aristocratCard);

                aristocratCardsNeeded--;
            }
        }

        public static void GetDevelopmentCards(DevelopmentCardTier tier, out List<DevelopmentCard> cards)
        {
            cards = null;

            switch (tier)
            {
                case DevelopmentCardTier.TIER1:
                    cards = new List<DevelopmentCard>(DevelopmentCardsTier1);
                    break;

                case DevelopmentCardTier.TIER2:
                    cards = new List<DevelopmentCard>(DevelopmentCardsTier2);
                    break;

                case DevelopmentCardTier.TIER3:
                    cards = new List<DevelopmentCard>(DevelopmentCardsTier3);
                    break;
            }
        }

        public static void GetJokerColor(out MarkerColor jokerColor)
        {
            jokerColor = JokerColor;
        }

        public static int GetNumberOfMarkersForPlayersCount(int playersCount)
        {
            return GameHardcodes.GetNumberOfMarkersForPlayersCount(playersCount);
        }

        public static int GetNumberOfJokers()
        {
            return GameHardcodes.MAX_NUMBER_OF_JOKERS;
        }

        public static int GetNumberOfAristocratsForPlayersCount(int playersCount)
        {
            int aristocratsCount = 3;

            if (playersCount > 2)
            {
                aristocratsCount = playersCount + 1;
            }

            if(aristocratsCount >= GameData.Aristocrats.Length)
            {
                aristocratsCount = GameData.Aristocrats.Length;
            }

            return aristocratsCount;
        }

        //public static 

        public static int GetMaxNumberOfPlayers()
        {
            return GameHardcodes.MAX_NUMBER_OF_PLAYERS;
        }

        public static int GetMinNumberOfPlayers()
        {
            return GameHardcodes.MIN_NUMBER_OF_PLAYERS;
        }
    }
}
