﻿using MainServerApp.Communication;
using MainServerApp.Communication.DataTypes;
using MainServerApp.GameItems;
using MainServerApp.Utils;
using MainServerApp.WebServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace MainServerApp.Managers
{
    class GameRoomManager
    {
        #region Room specific info
        public string Name;
        public string Alias;
        public int PlayersMaxCount;
        private LobbyManager ParentLobbyManager;
        #endregion

        #region Pre game-start info
        private Dictionary<string, PlayerEntity> PeopleInRoom;
        private Dictionary<string, bool> GameStartVotes;
        private int GameStartYesVotes;
        private bool GameStartVoteOngoing;
        private string GameStartVoteRequester;
        #endregion

        #region Game data
        private Queue<string> GameQueue;
        private PlayerEntity ActualTurnPlayer;
        private bool LastRound;
        private bool GameInProgress;
        #endregion

        #region Game resources
        #region Cards
        private List<DevelopmentCard> DevelopmentCardsTier1;
        private List<DevelopmentCard> DevelopmentCardsTier2;
        private List<DevelopmentCard> DevelopmentCardsTier3;
        private List<AristocratCard> AristocratCards;
        private List<DevelopmentCard> DevelopmentCardsDeck;
        #endregion
        #region Markers
        private Dictionary<MarkerColor, int> Markers;
        private MarkerColor JokerColor;
        #endregion
        #endregion

        public GameRoomManager(string name, string alias, int playersMax, LobbyManager lobbyManager)
        {
            Name = name;
            Alias = alias;
            PlayersMaxCount = playersMax;
            ParentLobbyManager = lobbyManager;
            PeopleInRoom = new Dictionary<string, PlayerEntity>();
            GameStartVotes = new Dictionary<string, bool>();

            GameStartVoteOngoing = false;
            LastRound = false;
            GameInProgress = false;

            DatabaseManager.AddGameRoom(alias);
        }

        public GameRoom CreateGameRoom()
        {
            return new GameRoom("anon#", this);
        }

        public GameRoomEntity GetRoomEntity()
        {
            return new GameRoomEntity(Name, Alias, PeopleInRoom.Count, PlayersMaxCount);
        }

        public void HandlePersonJoin(string nickname, GameRoom connectedGameRoom)
        {
            PeopleInRoom.Add(nickname, new PlayerEntity(connectedGameRoom));
        }

        public void HandlePersonLeave(string nickname)
        {
            PeopleInRoom.Remove(nickname);
        }

        public bool IsCardOnDeck(string cardId, string cardTier)
        {
            return (0 != DevelopmentCardsDeck.Where(card => card.GetCardId() == cardId && card.GetCardTierIdx().ToString() == cardTier).Count());
        }

        public List<DevelopmentCard> GetPlayerReservedCards(string playerName)
        {
            return PeopleInRoom[playerName].GetReservedCards();
        }

        public string GetOnlinePlayersMsg()
        {
            OnlinePlayersListMessage msg = new OnlinePlayersListMessage();

            foreach (KeyValuePair<string, PlayerEntity> player in PeopleInRoom)
            {
                msg.AddPlayer(player.Key);
            }

            string message = Newtonsoft.Json.JsonConvert.SerializeObject(msg);

            return message;
        }

        public bool IsGameInProgress()
        {
            return GameInProgress;
        }

        public bool IsCardInPlayerReservedCards(string name, string cardId, string cardTier)
        {
            return PeopleInRoom[name].IsCardInReservedCards(cardId, cardTier);
        }

        public bool CanPlayerAffordCard(DevelopmentCard devCard, string playerName)
        {
            return PeopleInRoom[playerName].CanPlayerAffordCard(devCard);
        }

        public DevelopmentCard GetPlayerReservedDevelopmentCard(string name, string cardId)
        {
            return PeopleInRoom[name].GetReservedCard(cardId);
        }

        public int GetMarkerCount(MarkerColor markerColor)
        {
            return Markers[markerColor];
        }

        public DevelopmentCard GetDevelopmentCard(string cardId)
        {
            return DevelopmentCardsDeck.Where(devCard => devCard.GetCardId() == cardId).First();
        }

        public bool IsGameStartVoteInProgress()
        {
            return GameStartVoteOngoing;
        }

        public void StartGameStartVote(string requester)
        {
            GameStartVoteOngoing = true;
            GameStartVoteRequester = requester;

            GameStartVotes = new Dictionary<string, bool>();
            GameStartYesVotes = 0;
        }

        public MarkerColor GetJokerColor()
        {
            return JokerColor;
        }

        public void AddGameStartVote(string voter, bool vote)
        {
            if (GameStartVotes.ContainsKey(voter))
                return;

            if (vote)
                GameStartYesVotes++;

            GameStartVotes.Add(voter, vote);

            if(GameStartVotes.Keys.Count == PeopleInRoom.Keys.Count)
            {
                // If there are only two players - all of them has to agree on staring the game
                // Otherwise at least half has to agree
                if((PeopleInRoom.Keys.Count == 2 && GameStartYesVotes == 2) ||
                   (GameStartYesVotes >= Math.Ceiling((double)(GameStartVotes.Keys.Count / 2)) && PeopleInRoom.Keys.Count > 2))
                {
                    //Game start prerequisites have been fulfilled - we can start the game
                    InitializeGame();
                }
                else
                {
                    //Game cannot be started
                    GameStartVoteFailure();
                }

                GameStartVoteOngoing = false;
            }
        }

        public void RequestBotForRoom()
        {
            BotServerManager.RequestBot(Alias);
        }

        private void GameStartVoteFailure()
        {
            foreach (KeyValuePair<string, PlayerEntity> player in PeopleInRoom)
            {
                player.Value.NotifyGameStartVoteFailure();
            }
        }

        public string GetGameStartVoteRequester()
        {
            return GameStartVoteRequester;
        }

        public void InitializeGame()
        {
            #region Initialize game resources
            GameManager.GetAristocratCards(PeopleInRoom.Count, out AristocratCards);
            GameManager.GetDevelopmentCards(DevelopmentCardTier.TIER1, out DevelopmentCardsTier1);
            GameManager.GetDevelopmentCards(DevelopmentCardTier.TIER2, out DevelopmentCardsTier2);
            GameManager.GetDevelopmentCards(DevelopmentCardTier.TIER3, out DevelopmentCardsTier3);

            DevelopmentCardsDeck = new List<DevelopmentCard>();

            GameManager.GetJokerColor(out JokerColor);

            Markers = new Dictionary<MarkerColor, int>();
            #endregion

            #region Populate markers decks
            int numberOfMarkers = GameManager.GetNumberOfMarkersForPlayersCount(PeopleInRoom.Count);

            foreach (MarkerColor marker in EnumUtils.GetValues<MarkerColor>())
            {
                if(marker == JokerColor)
                {
                    Markers.Add(marker, GameManager.GetNumberOfJokers());
                }
                else
                {
                    Markers.Add(marker, numberOfMarkers);
                }
            }
            #endregion

            #region Get 4 development card from each tier to deck
            PullNewDevelopmentCardsForDeck(4, DevelopmentCardsTier1);
            PullNewDevelopmentCardsForDeck(4, DevelopmentCardsTier2);
            PullNewDevelopmentCardsForDeck(4, DevelopmentCardsTier3);
            #endregion

            #region Initialize players
            Dictionary<string, PlayerInventoryEntity> playerInventories = new Dictionary<string, PlayerInventoryEntity>();
            foreach (KeyValuePair<string, PlayerEntity> player in PeopleInRoom)
            {
                player.Value.Initialize();
                playerInventories.Add(player.Key, player.Value.GetInventoryEntity());
            }

            FullGameStateUpdateMessage gameStartMessage = new FullGameStateUpdateMessage(EFullGameStateUpdateReason.GAME_START, AristocratCards, Markers, DevelopmentCardsDeck, JokerColor, null, null);
            foreach (KeyValuePair<string, PlayerEntity> player in PeopleInRoom)
            {
                PlayerInventoryEntity actualPlayerInventoryEntity = playerInventories[player.Key];

                playerInventories.Remove(player.Key);
                gameStartMessage.PlayerInventory = actualPlayerInventoryEntity;
                gameStartMessage.OtherPlayersInventories = new List<PlayerInventoryEntity>(playerInventories.Values);

                player.Value.SendFullGameStateUpdateEvent(gameStartMessage);

                playerInventories.Add(player.Key, actualPlayerInventoryEntity);
            }
            #endregion

            #region Queue up players
            GameQueue = new Queue<string>();

            int playersLeftToQueueUp = PeopleInRoom.Count;
            Random rnd = new Random();
            List<string> players = new List<string>(PeopleInRoom.Keys);

            while (playersLeftToQueueUp > 0)
            {
                string playerName = players[rnd.Next(players.Count)];

                while (GameQueue.Contains(playerName))
                {
                    playerName = players[rnd.Next(players.Count)];
                }

                GameQueue.Enqueue(playerName);

                playersLeftToQueueUp--;
            }
            #endregion

            #region Start first turn
            GameInProgress = true;

            NextPlayerTurn();
            #endregion
        }

        public FullGameStateUpdateMessage PrepareFullGameStateUpdateMessage(EFullGameStateUpdateReason reason, string name)
        {
            List<PlayerInventoryEntity> playerInventories = new List<PlayerInventoryEntity>();
            PlayerEntity actualPlayer = PeopleInRoom[name];
            foreach (KeyValuePair<string, PlayerEntity> player in PeopleInRoom)
            {
                if (player.Key == name) continue;
                playerInventories.Add(player.Value.GetInventoryEntity());
            }

            return new FullGameStateUpdateMessage(reason, AristocratCards, Markers, DevelopmentCardsDeck, JokerColor, actualPlayer.GetInventoryEntity(), playerInventories);
        }

        public void HandleTurnAction(string name, JToken turnActionToken)
        {
            DevelopmentCard devCard;

            //Update player inventory
            switch ((string)turnActionToken["Action"])
            {
                case "RESERVE_CARD":
                    devCard = PullDevelopmentCardFromDeck((string)turnActionToken["CardId"]);

                    UpdatePlayerReservedCards(name, devCard);

                    if(Markers[JokerColor] > 0)
                    {
                        UpdatePlayerMarker(name, JokerColor, 1);
                    }
                    break;

                case "BUY_CARD":
                    if(IsCardInPlayerReservedCards(name, (string)turnActionToken["CardId"], (string)turnActionToken["CardTier"]))
                    {
                        PeopleInRoom[name].BuyReservedCard((string)turnActionToken["CardId"]);
                    }
                    else
                    {
                        devCard = PullDevelopmentCardFromDeck((string)turnActionToken["CardId"]);

                        UpdatePlayerCards(name, devCard);
                    }
                    break;

                case "TAKE_MARKERS":
                    JProperty takenMarker = (JProperty)((JContainer)(turnActionToken["TakenMarkers"])).First;
                    while (takenMarker != null)
                    {
                        MarkerColor markerColor = EnumUtils.GetEnumValueFromName<MarkerColor>(takenMarker.Name);
                        UpdatePlayerMarker(name, markerColor, (int)takenMarker.Value);

                        takenMarker = (JProperty)takenMarker.Next;
                    }
                    break;

                case "PASS":
                    break;
            }
            //Conclude turn
            ConcludeTurn();
        }

        internal string GetAlias()
        {
            return Alias;
        }

        private void UpdatePlayerReservedCards(string name, DevelopmentCard devCard)
        {
            PeopleInRoom[name].AddReservedDevelopmentCard(devCard);
        }

        internal bool IsItMyTurn(GameRoom gameRoom)
        {
            bool retVal = false;

            foreach(KeyValuePair<string, PlayerEntity> player in PeopleInRoom)
            {
                if(player.Value.GetGameRoom() == gameRoom)
                {
                    retVal = true;
                    break;
                }
            }

            return retVal;
        }

        private void UpdatePlayerCards(string name, DevelopmentCard devCard)
        {
            foreach(KeyValuePair<MarkerColor, int> marker in devCard.GetConsumedMarkers())
            {
                PeopleInRoom[name].UpdatePlayerMarker(marker.Key, (-1) * marker.Value);
                Markers[marker.Key] += marker.Value;
            }
            PeopleInRoom[name].AddDevelopmentCard(devCard);
        }

        private void UpdatePlayerMarker(string name, MarkerColor markerColor, int value)
        {
            Markers[markerColor] -= value;
            PeopleInRoom[name].UpdatePlayerMarker(markerColor, value);
        }

        private DevelopmentCard PullDevelopmentCardFromDeck(string cardId)
        {
            DevelopmentCard devCard = DevelopmentCardsDeck.Where(dc => dc.GetCardId() == cardId).First();

            DevelopmentCardsDeck.Remove(devCard);

            if      (devCard.GetCardTierIdx() == 0) PullNewDevelopmentCardsForDeck(1, DevelopmentCardsTier1);
            else if (devCard.GetCardTierIdx() == 1) PullNewDevelopmentCardsForDeck(1, DevelopmentCardsTier2);
            else if (devCard.GetCardTierIdx() == 2) PullNewDevelopmentCardsForDeck(1, DevelopmentCardsTier3);

            return devCard;
        }

        private void PullNewDevelopmentCardsForDeck(int cardsToPull, List<DevelopmentCard> sourceDeck)
        {
            Random rnd = new Random();

            while (cardsToPull > 0)
            {
                DevelopmentCard developmentCard = sourceDeck[rnd.Next(sourceDeck.Count)];

                DevelopmentCardsDeck.Add(developmentCard);
                sourceDeck.Remove(developmentCard);

                cardsToPull--;
            }
        }

        public void ConcludeTurn()
        {
            #region Handle Aristocrats visits
            Dictionary<MarkerColor, int> playerOwnedBonuses;
            AristocratCard cardToAdd = null;

            ActualTurnPlayer.GetOwnedBonuses(out playerOwnedBonuses);

            foreach(AristocratCard aristocrat in AristocratCards)
            {
                if(aristocrat.AreRequirementsMet(playerOwnedBonuses))
                {
                    cardToAdd = aristocrat;
                    break;
                }
            }

            if(cardToAdd != null)
            {
                ActualTurnPlayer.AddAristocratCard(cardToAdd);
                AristocratCards.Remove(cardToAdd);
            }
            #endregion

            //TODO: Do partial update here
            foreach (KeyValuePair<string, PlayerEntity> player in PeopleInRoom)
            {
                FullGameStateUpdateMessage updateMessage = PrepareFullGameStateUpdateMessage(EFullGameStateUpdateReason.NEW_TURN, player.Key);
                player.Value.SendFullGameStateUpdateEvent(updateMessage);
            }

            NextPlayerTurn();
        }

        private void NextPlayerTurn()
        {
            if(GameQueue.Count == 0)
            {
                EndGame();
                return;
            }

            bool playerIsStillConnected = false;

            while(!playerIsStillConnected)
            {
                string nextPlayer = GameQueue.Dequeue();

                playerIsStillConnected = PeopleInRoom.ContainsKey(nextPlayer);

                if(playerIsStillConnected)
                {
                    ActualTurnPlayer = PeopleInRoom[nextPlayer];

                    ActualTurnPlayer.NotifyTurn();

                    if(!LastRound)
                    {
                        GameQueue.Enqueue(nextPlayer);
                    }
                }
                else if (GameQueue.Count == 0)
                {
                    break;
                }
            }

            // If all other players that were supposed to make their turn now have disconnnected, we should end the game
            if (!playerIsStillConnected)
            {
                EndGame();
                return;
            }
        }

        private void EndGame()
        {
            //TODO: End the game
        }

        public LobbyManager GetParentLobbyManager()
        {
            return ParentLobbyManager;
        }
    }
}
