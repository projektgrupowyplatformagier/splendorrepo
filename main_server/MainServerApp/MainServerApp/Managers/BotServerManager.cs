﻿using MainServerApp.Communication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebSocketSharp;

namespace MainServerApp.Managers
{
    public static class BotServerManager
    {
        private static List<string> AvailableBotServers = new List<string>();

        public static void AddBotServer(string serverAddress)
        {
            AvailableBotServers.Add(serverAddress);
        }

        public static void RequestBot(string roomName)
        {
            // TODO Rework this
            using (var ws = new WebSocket(AvailableBotServers[0]))
            {
                ws.Connect();
                BotServerBotRequest br = new BotServerBotRequest("ws://127.0.0.1:4649/" + roomName);
                ws.Send(Newtonsoft.Json.JsonConvert.SerializeObject(br));
                ws.Close();
            }
        }
    }
}
