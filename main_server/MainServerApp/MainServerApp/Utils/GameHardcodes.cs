﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainServerApp.Utils
{
    public static class GameHardcodes
    {
        public const int MAX_NUMBER_OF_MARKERS = 7;
        public const int MAX_NUMBER_OF_JOKERS = 5;
        public const int MAX_NUMBER_OF_PLAYERS = 5;
        public const int MIN_NUMBER_OF_PLAYERS = 2;

        public static int GetNumberOfMarkersForPlayersCount(int playersCount)
        {
            int markersCountToSubtract;

            if(playersCount > 5)
            {
                markersCountToSubtract = 0;
            }
            else if(playersCount > 2)
            {
                markersCountToSubtract = 5 - playersCount;
            }
            else
            {
                markersCountToSubtract = 3;
            }

            return (MAX_NUMBER_OF_MARKERS - markersCountToSubtract);
        }

    }
}
