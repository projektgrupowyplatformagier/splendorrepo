﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainServerApp.Utils
{
    public static class EnumUtils
    {
        public static IEnumerable<T> GetValues<T>()
        {
            return Enum.GetValues(typeof(T)).Cast<T>();
        }

        public static T GetEnumValueFromName<T>(string name)
        {
            foreach (T value in EnumUtils.GetValues<T>())
            {
                if(String.Equals(value.ToString(), name, StringComparison.OrdinalIgnoreCase))
                {
                    return value;
                }
            }

            return default(T);
        }

        public static bool IsValueFromEnum<T>(string name)
        {
            foreach (T value in EnumUtils.GetValues<T>())
            {
                if (String.Equals(value.ToString(), name, StringComparison.OrdinalIgnoreCase))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
