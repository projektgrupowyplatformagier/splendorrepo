﻿using MainServerApp.Communication;
using MainServerApp.GameItems;
using MainServerApp.Managers;
using MainServerApp.Utils;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebSocketSharp;
using WebSocketSharp.Server;

namespace MainServerApp.WebServices
{
    class GameRoom : WebSocketBehavior
    {
        private string Name;
        private static int Number = 0;
        private string Prefix;
        private GameRoomManager ParentManager;
        private bool UserValid = false;
        private string UserUid = null;

        public GameRoom()
            : this(null)
        {
        }

        public GameRoom(string prefix)
        {
            Prefix = !prefix.IsNullOrEmpty() ? prefix : "anon#";
        }

        public GameRoom(string prefix, GameRoomManager manager) : this(prefix)
        {
            this.ParentManager = manager;
        }

        private string GetName()
        {
            var userId = Context.QueryString["uid"];
            var botName = Context.QueryString["bot_name"];
            string username = null;
            if (!botName.IsNullOrEmpty())
            {
                username = botName;
            }
            else if (!userId.IsNullOrEmpty())
            {
                username = DatabaseManager.GetUsernameFromId(userId);
                UserUid = userId;
            }

            //var name = Context.QueryString["name"];
            return !username.IsNullOrEmpty() ? username : Prefix + GetNumber();
        }

        private static int GetNumber()
        {
            return Interlocked.Increment(ref Number);
        }

        public string GetPlayerName()
        {
            return Name;
        }

        protected override void OnClose(CloseEventArgs e)
        {
            if (!UserValid) return;

            ParentManager.HandlePersonLeave(Name);

            ChatMessage msg = new ChatMessage(String.Format("{0} got logged off...", Name), ChatSenderType.SERVER, "");
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(msg);

            Sessions.Broadcast(json);

            Sessions.Broadcast(ParentManager.GetOnlinePlayersMsg());
        }

        public void NotifyTurn()
        {
            TurnNotificationMessage msg = new TurnNotificationMessage();
            string outJson = Newtonsoft.Json.JsonConvert.SerializeObject(msg);

            Send(outJson);
        }

        private bool ValidateTurnAction(JToken turnActionToken)
        {
            bool turnActionValid = false;
            bool cardIsOnDeck = false;
            bool cardInPlayerReservedCards = false;
            if (turnActionToken["Action"] == null) return false;

            switch ((string)turnActionToken["Action"])
            {
                case "RESERVE_CARD":
                    if (turnActionToken["CardId"] == null) break;
                    if (turnActionToken["CardTier"] == null) break;

                    //To check here:
                    //1. Is card available on deck?
                    //2. Does player has less than three reserved cards in his hand?
                    cardIsOnDeck = ParentManager.IsCardOnDeck((string)turnActionToken["CardId"], (string)turnActionToken["CardTier"]);

                    if (cardIsOnDeck)
                    {
                        if (ParentManager.GetPlayerReservedCards(Name).Count < 3)
                        {
                            turnActionValid = true;
                        }
                    }
                    break;

                case "BUY_CARD":
                    if (turnActionToken["CardId"] == null) break;
                    if (turnActionToken["CardTier"] == null) break;

                    //To check here:
                    //1. Is card available on deck?
                    //2. Can he afford the card?
                    cardIsOnDeck = ParentManager.IsCardOnDeck((string)turnActionToken["CardId"], (string)turnActionToken["CardTier"]);
                    cardInPlayerReservedCards = ParentManager.IsCardInPlayerReservedCards(Name, (string)turnActionToken["CardId"], (string)turnActionToken["CardTier"]);

                    if (cardIsOnDeck)
                    {
                        if (ParentManager.CanPlayerAffordCard(ParentManager.GetDevelopmentCard((string)turnActionToken["CardId"]), Name))
                        {
                            turnActionValid = true;
                        }
                    }
                    else if(cardInPlayerReservedCards)
                    {
                        if (ParentManager.CanPlayerAffordCard(ParentManager.GetPlayerReservedDevelopmentCard(Name, (string)turnActionToken["CardId"]), Name))
                        {
                            turnActionValid = true;
                        }
                    }
                    break;

                case "TAKE_MARKERS":
                    if (turnActionToken["TakenMarkers"] == null) break;

                    if(((JContainer)(turnActionToken["TakenMarkers"])).Count == 1)
                    {
                        MarkerColor markerColor = default(MarkerColor);

                        JProperty takenMarker = (JProperty)((JContainer)(turnActionToken["TakenMarkers"])).First;
                        if ((int)takenMarker.Value == 2 && EnumUtils.IsValueFromEnum<MarkerColor>(takenMarker.Name))
                        {
                            markerColor = EnumUtils.GetEnumValueFromName<MarkerColor>(takenMarker.Name);
                            if(markerColor != ParentManager.GetJokerColor())
                            {
                                turnActionValid = true;
                            }
                        }

                        if (turnActionValid)
                        {
                            if (ParentManager.GetMarkerCount(markerColor) < 4)
                            {
                                turnActionValid = false;
                            }
                        }
                    }
                    else if (((JContainer)(turnActionToken["TakenMarkers"])).Count == 3)
                    {
                        MarkerColor markerColor = default(MarkerColor);
                        turnActionValid = true;

                        JProperty takenMarker = (JProperty)((JContainer)(turnActionToken["TakenMarkers"])).First;
                        while(takenMarker != null)
                        {
                            if ((int)takenMarker.Value != 1 || !EnumUtils.IsValueFromEnum<MarkerColor>(takenMarker.Name))
                            {
                                turnActionValid = false;
                                break;
                            }
                            else
                            {
                                markerColor = EnumUtils.GetEnumValueFromName<MarkerColor>(takenMarker.Name);
                                if(ParentManager.GetMarkerCount(markerColor) < 1 || markerColor == ParentManager.GetJokerColor())
                                {
                                    turnActionValid = false;
                                    break;
                                }
                            }

                            takenMarker = (JProperty)takenMarker.Next;
                        }
                    }
                    break;

                case "PASS":
                    turnActionValid = true;
                    break;
            }

            return turnActionValid;
        }

        protected override void OnMessage(MessageEventArgs e)
        {
            if (!UserValid) return;

            JToken token = JObject.Parse(e.Data);
            string outJson;

            switch ((string)token["Type"])
            {
                case "CHAT":
                    ChatMessage chatMsg = new ChatMessage((string)token["Message"], ChatSenderType.USER, Name);
                    outJson = Newtonsoft.Json.JsonConvert.SerializeObject(chatMsg);

                    Sessions.Broadcast(outJson);
                    break;

                case "PLAYERS_ONLINE_REQUEST":
                    Send(ParentManager.GetOnlinePlayersMsg());
                    break;

                case "GAME_START_VOTE_REQUEST":
                    GameStartVoteRequest gameStartVoteReqMsg;

                    if (ParentManager.IsGameStartVoteInProgress())
                    {
                        //Send to this exact client info that vote is ongoing
                        ParentManager.StartGameStartVote(Name);

                        gameStartVoteReqMsg = new GameStartVoteRequest(Name);
                        outJson = Newtonsoft.Json.JsonConvert.SerializeObject(gameStartVoteReqMsg);

                        Send(outJson);
                    }
                    else if (ParentManager.IsGameInProgress())
                    {
                        //Send info that the game is ongoing along with game data
                    }
                    else
                    {
                        //Broadcast game vote request
                        ParentManager.StartGameStartVote(Name);

                        gameStartVoteReqMsg = new GameStartVoteRequest(Name);
                        outJson = Newtonsoft.Json.JsonConvert.SerializeObject(gameStartVoteReqMsg);

                        Sessions.Broadcast(outJson);
                    }
                    break;

                case "GAME_START_VOTE":
                    ParentManager.AddGameStartVote(Name, (bool)token["Vote"]);
                    break;

                case "REQUEST_BOT_REQUEST":
                    ParentManager.RequestBotForRoom();
                    break;

                case "TURN_ACTION":
                    if (ValidateTurnAction(token))
                    {
                        //Process turn
                        ParentManager.HandleTurnAction(Name, token);
                    }
                    else
                    {
                        //TODO: Send to player FULL_GAME_STATE_UPDATE with INVALID_TURN_ACTION reason
                        FullGameStateUpdateMessage fullUpdateMessage = ParentManager.PrepareFullGameStateUpdateMessage(EFullGameStateUpdateReason.INVALID_TURN_ACTION_ROLLBACK, Name);
                        outJson = Newtonsoft.Json.JsonConvert.SerializeObject(fullUpdateMessage);

                        Send(outJson);

                        NotifyTurn();
                    }
                    break;

                default:
                    break;
            }
        }

        public void NotifyGameStartVoteFailure()
        {
            GameStartVoteFailureMessage msg = new GameStartVoteFailureMessage();
            string outJson = Newtonsoft.Json.JsonConvert.SerializeObject(msg);

            Send(outJson);
        }

        public void SendFullGameStateUpdateEvent(FullGameStateUpdateMessage gameStartEvent)
        {
            string outJson = Newtonsoft.Json.JsonConvert.SerializeObject(gameStartEvent);

            Send(outJson);
        }

        protected override void OnOpen()
        {
            Name = GetName();
            string gameRoomAlias = null;

            if (!Context.QueryString["uid"].IsNullOrEmpty())
            {
                if (!DatabaseManager.IsUserInGameRoom(Context.QueryString["uid"], out gameRoomAlias))
                {
                    gameRoomAlias = null;
                }
            }

            if (gameRoomAlias != null && gameRoomAlias != ParentManager.GetAlias() && ParentManager.GetParentLobbyManager().DoesGameRoomExist(gameRoomAlias))
            {
                Send(ParentManager.GetParentLobbyManager().GetGotoGameRoomMsg(gameRoomAlias));
            }
            else
            {
                UserValid = true;

                if(UserUid != null)
                {
                    DatabaseManager.AddUserToGameRoom(UserUid, ParentManager.GetAlias());
                }

                ParentManager.HandlePersonJoin(Name, this);

                ChatMessage msg = new ChatMessage(String.Format("{0} has logged in...", Name), ChatSenderType.SERVER, "");
                var json = Newtonsoft.Json.JsonConvert.SerializeObject(msg);
                Sessions.Broadcast(json);

                ChatMessage msg2 = new ChatMessage("You have joined the chat", ChatSenderType.SERVER, "");
                var json2 = Newtonsoft.Json.JsonConvert.SerializeObject(msg2);
                Send(json2);

                if (gameRoomAlias != null && gameRoomAlias == ParentManager.GetAlias() && ParentManager.GetParentLobbyManager().DoesGameRoomExist(gameRoomAlias))
                {
                    FullGameStateUpdateMessage fullUpdateMessage = ParentManager.PrepareFullGameStateUpdateMessage(EFullGameStateUpdateReason.INVALID_TURN_ACTION_ROLLBACK, Name);
                    var outJson = Newtonsoft.Json.JsonConvert.SerializeObject(fullUpdateMessage);

                    Send(outJson);

                    if(ParentManager.IsItMyTurn(this))
                        NotifyTurn();
                }

                Sessions.Broadcast(ParentManager.GetOnlinePlayersMsg());
            }
        }
    }
}
