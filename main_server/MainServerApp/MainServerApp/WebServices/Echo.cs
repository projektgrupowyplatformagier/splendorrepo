﻿using System;
using WebSocketSharp;
using WebSocketSharp.Server;

namespace MainServerApp.WebServices
{
    public class Echo : WebSocketBehavior
    {
        protected override void OnMessage(MessageEventArgs e)
        {
            var name = Context.QueryString["name"];
            Send(!name.IsNullOrEmpty() ? String.Format("\"{0}\" to {1}", e.Data, name) : e.Data);
        }

        protected override void OnClose(CloseEventArgs e)
        {
            base.OnClose(e);
            Console.WriteLine("Closing connection");
        }

        protected override void OnOpen()
        {
            base.OnOpen();
            Console.WriteLine("Opening");
        }
    }
}
