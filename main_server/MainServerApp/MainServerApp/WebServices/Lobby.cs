﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebSocketSharp;
using WebSocketSharp.Server;
using System.Threading;
using MainServerApp.Managers;
using MainServerApp.Communication;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MainServerApp.WebServices
{
    class Lobby : WebSocketBehavior
    {
        private string _name;
        private static int _number = 0;
        private string _prefix;
        private LobbyManager ParentLobbyManager;
        private bool UserValid = false;

        public Lobby()
            : this(null)
        {
        }

        public Lobby(string prefix)
        {
            _prefix = !prefix.IsNullOrEmpty() ? prefix : "anon#";
        }

        public Lobby(string prefix, LobbyManager lobbyManager) : this(prefix)
        {
            this.ParentLobbyManager = lobbyManager;
        }

        private string GetName()
        {
            var userId = Context.QueryString["uid"];
            string username = null;
            if(!userId.IsNullOrEmpty()) username = DatabaseManager.GetUsernameFromId(userId);

            //var name = Context.QueryString["name"];
            return !username.IsNullOrEmpty() ? username : _prefix + GetNumber();
        }

        private static int GetNumber()
        {
            return Interlocked.Increment(ref _number);
        }

        protected override void OnClose(CloseEventArgs e)
        {
            if (!UserValid) return;

            ParentLobbyManager.HandlePersonLeave(_name);

            ChatMessage msg = new ChatMessage(String.Format("{0} got logged off...", _name), ChatSenderType.SERVER, "");
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(msg);

            Sessions.Broadcast(json);

            Sessions.Broadcast(ParentLobbyManager.GetOnlinePlayersMsg());
        }

        protected override void OnMessage(MessageEventArgs e)
        {
            if (!UserValid) return;

            JToken token = JObject.Parse(e.Data);

            switch((string)token["Type"])
            {
                case "CHAT":
                    ChatMessage msg = new ChatMessage((string)token["Message"], ChatSenderType.USER, _name);
                    var json = Newtonsoft.Json.JsonConvert.SerializeObject(msg);

                    Sessions.Broadcast(json);
                    break;

                case "GAME_ROOMS_REQUEST":
                    Send(ParentLobbyManager.GetGameRoomsMsg());
                    break;

                case "PLAYERS_ONLINE_REQUEST":
                    Send(ParentLobbyManager.GetOnlinePlayersMsg());
                    break;

                case "CREATE_GAME_ROOM_REQUEST":
                    ParentLobbyManager.CreateGameRoom();
                    Sessions.Broadcast(ParentLobbyManager.GetGameRoomsMsg());
                    break;

                default:
                    break;
            }
        }

        protected override void OnOpen()
        {
            _name = GetName();
            string gameRoomAlias = null;

            if (!Context.QueryString["uid"].IsNullOrEmpty())
            {
                if(!DatabaseManager.IsUserInGameRoom(Context.QueryString["uid"], out gameRoomAlias))
                {
                    gameRoomAlias = null;
                }
            }

            if (gameRoomAlias != null && ParentLobbyManager.DoesGameRoomExist(gameRoomAlias))
            {
                Send(ParentLobbyManager.GetGotoGameRoomMsg(gameRoomAlias));
            }
            else
            {
                UserValid = true;

                ParentLobbyManager.HandlePersonJoin(_name);

                ChatMessage msg = new ChatMessage(String.Format("{0} has logged in...", _name), ChatSenderType.SERVER, "");
                var json = Newtonsoft.Json.JsonConvert.SerializeObject(msg);
                Sessions.Broadcast(json);

                ChatMessage msg2 = new ChatMessage("You have joined the chat", ChatSenderType.SERVER, "");
                var json2 = Newtonsoft.Json.JsonConvert.SerializeObject(msg2);
                Send(json2);

                Sessions.Broadcast(ParentLobbyManager.GetOnlinePlayersMsg());
            }
        }
    }
}
