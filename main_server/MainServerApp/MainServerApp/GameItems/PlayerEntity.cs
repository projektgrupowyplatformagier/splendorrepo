﻿using MainServerApp.Communication;
using MainServerApp.Utils;
using MainServerApp.WebServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MainServerApp.Communication.DataTypes;

namespace MainServerApp.GameItems
{
    class PlayerEntity
    {
        private List<DevelopmentCard> DevelopmentCards;
        private List<DevelopmentCard> ReservedDevelopmentCards;
        private List<AristocratCard> AristocratCards;
        private Dictionary<MarkerColor, int> Markers;
        private Dictionary<MarkerColor, int> Bonuses;
        private int MarkersOverallCount;
        private GameRoom ConnectedGameRoom;

        public PlayerEntity(GameRoom connectedGameRoom)
        {
            ConnectedGameRoom = connectedGameRoom;
        }

        public void Initialize()
        {
            DevelopmentCards = new List<DevelopmentCard>();
            ReservedDevelopmentCards = new List<DevelopmentCard>();
            AristocratCards = new List<AristocratCard>();
            Markers = new Dictionary<MarkerColor, int>();
            Bonuses = new Dictionary<MarkerColor, int>();

            InitializeMarkersAndBonuses();
        }

        private void InitializeMarkersAndBonuses()
        {
            foreach(MarkerColor marker in EnumUtils.GetValues<MarkerColor>())
            {
                Markers.Add(marker, 0);
                Bonuses.Add(marker, 0);
            }
        }

        public void NotifyTurn()
        {
            ConnectedGameRoom.NotifyTurn();
        }

        public void GetOwnedBonuses(out Dictionary<MarkerColor, int> ownedBonuses)
        {
            ownedBonuses = new Dictionary<MarkerColor, int>(Bonuses);
        }

        public void GetCollectedMarkers(out Dictionary<MarkerColor, int> ownedBonuses)
        {
            ownedBonuses = new Dictionary<MarkerColor, int>(Bonuses);
        }

        public void AddAristocratCard(AristocratCard aristocratCard)
        {
            AristocratCards.Add(aristocratCard);
        }

        public void SendFullGameStateUpdateEvent(FullGameStateUpdateMessage fullGameStateUpdateEvent)
        {
            ConnectedGameRoom.SendFullGameStateUpdateEvent(fullGameStateUpdateEvent);
        }

        public void NotifyGameStartVoteFailure()
        {
            ConnectedGameRoom.NotifyGameStartVoteFailure();
        }

        public PlayerInventoryEntity GetInventoryEntity()
        {
            return new PlayerInventoryEntity(ConnectedGameRoom.GetPlayerName(), AristocratCards, Markers, DevelopmentCards, ReservedDevelopmentCards);
        }

        public void UpdatePlayerMarker(MarkerColor markerColor, int value)
        {
            Markers[markerColor] += value;
        }

        public List<DevelopmentCard> GetReservedCards()
        {
            return ReservedDevelopmentCards;
        }

        public int GetMarkerCount(MarkerColor key)
        {
            return Bonuses[key] + Markers[key];
        }

        public void AddReservedDevelopmentCard(DevelopmentCard devCard)
        {
            ReservedDevelopmentCards.Add(devCard);
        }

        public void AddDevelopmentCard(DevelopmentCard devCard)
        {
            DevelopmentCards.Add(devCard);
        }

        public bool IsCardInReservedCards(string cardId, string cardTier)
        {
            return (0 != ReservedDevelopmentCards.Where(card => card.GetCardId() == cardId && card.GetCardTierIdx().ToString() == cardTier).Count());
        }

        public DevelopmentCard GetReservedCard(string cardId)
        {
            return ReservedDevelopmentCards.Where(card => card.GetCardId() == cardId).First();
        }

        public void BuyReservedCard(string cardId)
        {
            DevelopmentCard devCard = GetReservedCard(cardId);

            ReservedDevelopmentCards.Remove(devCard);
            DevelopmentCards.Add(devCard);
        }

        internal GameRoom GetGameRoom()
        {
            return ConnectedGameRoom;
        }
        private int GetGoldMarkersCount()
        {
            return Markers[MarkerColor.GoldMarker];
        }

        private int GetBlackMarkersCount()
        {
            return Markers[MarkerColor.BlackMarker] + GatherCertainMarkersFromDevCards(MarkerColor.BlackMarker);
        }
        private int GetBlueMarkersCount()
        {
            return Markers[MarkerColor.BlueMarker] + GatherCertainMarkersFromDevCards(MarkerColor.BlueMarker);
        }
        private int GetRedMarkersCount()
        {
            return Markers[MarkerColor.RedMarker] + GatherCertainMarkersFromDevCards(MarkerColor.RedMarker);
        }
        private int GetWhiteMarkersCount()
        {
            return Markers[MarkerColor.WhiteMarker] + GatherCertainMarkersFromDevCards(MarkerColor.WhiteMarker);
        }
        private int GetGreenMarkersCount()
        {
            return Markers[MarkerColor.GreenMarker] + GatherCertainMarkersFromDevCards(MarkerColor.GreenMarker);
        }
        private int GatherCertainMarkersFromDevCards(MarkerColor marketColor)
        {
            if (DevelopmentCards.Count > 0)
            {
                int markers = 0;
                foreach (DevelopmentCard developmentCard in DevelopmentCards)
                {
                    if (developmentCard.GetProducedMarker() == marketColor)
                    {
                        markers++;
                    }
                }
                return markers;
            }
            else
            {
                return 0;
            }
        }
        public bool CanPlayerAffordCard(DevelopmentCard devCard)
        {
            int requiredBlueMarkers, requiredGreenMarkers, requiredWhiteMarkers, requiredRedMarkers, requiredBlackMarkers;
            try
            {
                requiredBlueMarkers = devCard.GetConsumedMarkers()[MarkerColor.BlueMarker];
            }
            catch
            {
                requiredBlueMarkers = 0;
            }
            try
            {
                requiredGreenMarkers = devCard.GetConsumedMarkers()[MarkerColor.GreenMarker];
            }
            catch
            {
                requiredGreenMarkers = 0;
            }
            try
            {
                requiredWhiteMarkers = devCard.GetConsumedMarkers()[MarkerColor.WhiteMarker];
            }
            catch
            {
                requiredWhiteMarkers = 0;
            }
            try
            {
                requiredRedMarkers = devCard.GetConsumedMarkers()[MarkerColor.RedMarker];
            }
            catch
            {
                requiredRedMarkers = 0;
            }
            try
            {
                requiredBlackMarkers = devCard.GetConsumedMarkers()[MarkerColor.BlackMarker];
            }
            catch
            {
                requiredBlackMarkers = 0;
            }

            // Try without GoldMarkers
            if (requiredBlackMarkers <= GetBlackMarkersCount() &&
                requiredBlueMarkers <= GetBlueMarkersCount() &&
                requiredGreenMarkers <= GetGreenMarkersCount() &&
                requiredRedMarkers <= GetRedMarkersCount() &&
                requiredWhiteMarkers <= GetWhiteMarkersCount())
            {
                return true;
            }
            else
            {
                // Try with GoldMarkers
                switch (GetGoldMarkersCount())
                {
                    case 3:
                        // TBD: Figure out a more cleaver way of checking markers variants
                    case 2:
                        return (
                            (requiredBlackMarkers <= GetBlackMarkersCount() + 2 &&
                            requiredBlueMarkers <= GetBlueMarkersCount() &&
                            requiredGreenMarkers <= GetGreenMarkersCount() &&
                            requiredRedMarkers <= GetRedMarkersCount() &&
                            requiredWhiteMarkers <= GetWhiteMarkersCount())
                            ||
                            (requiredBlackMarkers <= GetBlackMarkersCount() + 1 &&
                            requiredBlueMarkers <= GetBlueMarkersCount() + 1 &&
                            requiredGreenMarkers <= GetGreenMarkersCount() &&
                            requiredRedMarkers <= GetRedMarkersCount() &&
                            requiredWhiteMarkers <= GetWhiteMarkersCount())
                            ||
                            (requiredBlackMarkers <= GetBlackMarkersCount() + 1 &&
                            requiredBlueMarkers <= GetBlueMarkersCount() &&
                            requiredGreenMarkers <= GetGreenMarkersCount() + 1 &&
                            requiredRedMarkers <= GetRedMarkersCount() &&
                            requiredWhiteMarkers <= GetWhiteMarkersCount())
                            ||
                            (requiredBlackMarkers <= GetBlackMarkersCount() + 1 &&
                            requiredBlueMarkers <= GetBlueMarkersCount() &&
                            requiredGreenMarkers <= GetGreenMarkersCount() &&
                            requiredRedMarkers <= GetRedMarkersCount() + 1 &&
                            requiredWhiteMarkers <= GetWhiteMarkersCount())
                            ||
                            (requiredBlackMarkers <= GetBlackMarkersCount() + 1 &&
                            requiredBlueMarkers <= GetBlueMarkersCount() &&
                            requiredGreenMarkers <= GetGreenMarkersCount() &&
                            requiredRedMarkers <= GetRedMarkersCount() &&
                            requiredWhiteMarkers <= GetWhiteMarkersCount() + 1)
                            ||
                            (requiredBlackMarkers <= GetBlackMarkersCount() &&
                            requiredBlueMarkers <= GetBlueMarkersCount() + 2 &&
                            requiredGreenMarkers <= GetGreenMarkersCount() &&
                            requiredRedMarkers <= GetRedMarkersCount() &&
                            requiredWhiteMarkers <= GetWhiteMarkersCount())
                            ||
                            (requiredBlackMarkers <= GetBlackMarkersCount() &&
                            requiredBlueMarkers <= GetBlueMarkersCount() + 1 &&
                            requiredGreenMarkers <= GetGreenMarkersCount() + 1 &&
                            requiredRedMarkers <= GetRedMarkersCount() &&
                            requiredWhiteMarkers <= GetWhiteMarkersCount())
                            ||
                            (requiredBlackMarkers <= GetBlackMarkersCount() &&
                            requiredBlueMarkers <= GetBlueMarkersCount() + 1 &&
                            requiredGreenMarkers <= GetGreenMarkersCount() &&
                            requiredRedMarkers <= GetRedMarkersCount() + 1 &&
                            requiredWhiteMarkers <= GetWhiteMarkersCount())
                            ||
                            (requiredBlackMarkers <= GetBlackMarkersCount() &&
                            requiredBlueMarkers <= GetBlueMarkersCount() + 1 &&
                            requiredGreenMarkers <= GetGreenMarkersCount() &&
                            requiredRedMarkers <= GetRedMarkersCount() &&
                            requiredWhiteMarkers <= GetWhiteMarkersCount() + 1)
                            ||
                            (requiredBlackMarkers <= GetBlackMarkersCount() &&
                            requiredBlueMarkers <= GetBlueMarkersCount() &&
                            requiredGreenMarkers <= GetGreenMarkersCount() + 2 &&
                            requiredRedMarkers <= GetRedMarkersCount() &&
                            requiredWhiteMarkers <= GetWhiteMarkersCount())
                            ||
                            (requiredBlackMarkers <= GetBlackMarkersCount() &&
                            requiredBlueMarkers <= GetBlueMarkersCount() &&
                            requiredGreenMarkers <= GetGreenMarkersCount() + 1 &&
                            requiredRedMarkers <= GetRedMarkersCount() + 1 &&
                            requiredWhiteMarkers <= GetWhiteMarkersCount())
                            ||
                            (requiredBlackMarkers <= GetBlackMarkersCount() &&
                            requiredBlueMarkers <= GetBlueMarkersCount() &&
                            requiredGreenMarkers <= GetGreenMarkersCount() + 1 &&
                            requiredRedMarkers <= GetRedMarkersCount() &&
                            requiredWhiteMarkers <= GetWhiteMarkersCount() + 1)
                            ||
                            (requiredBlackMarkers <= GetBlackMarkersCount() &&
                            requiredBlueMarkers <= GetBlueMarkersCount() &&
                            requiredGreenMarkers <= GetGreenMarkersCount() &&
                            requiredRedMarkers <= GetRedMarkersCount() + 2 &&
                            requiredWhiteMarkers <= GetWhiteMarkersCount())
                            ||
                            (requiredBlackMarkers <= GetBlackMarkersCount() &&
                            requiredBlueMarkers <= GetBlueMarkersCount() &&
                            requiredGreenMarkers <= GetGreenMarkersCount() &&
                            requiredRedMarkers <= GetRedMarkersCount() + 1 &&
                            requiredWhiteMarkers <= GetWhiteMarkersCount() + 1)
                            ||
                            (requiredBlackMarkers <= GetBlackMarkersCount() &&
                            requiredBlueMarkers <= GetBlueMarkersCount() &&
                            requiredGreenMarkers <= GetGreenMarkersCount() &&
                            requiredRedMarkers <= GetRedMarkersCount() &&
                            requiredWhiteMarkers <= GetWhiteMarkersCount() + 2)
                            );
                    case 1:
                        return (
                            (requiredBlackMarkers <= GetBlackMarkersCount() + 1 &&
                            requiredBlueMarkers <= GetBlueMarkersCount() &&
                            requiredGreenMarkers <= GetGreenMarkersCount() &&
                            requiredRedMarkers <= GetRedMarkersCount() &&
                            requiredWhiteMarkers <= GetWhiteMarkersCount())
                            ||
                            (requiredBlackMarkers <= GetBlackMarkersCount() &&
                            requiredBlueMarkers <= GetBlueMarkersCount() + 1 &&
                            requiredGreenMarkers <= GetGreenMarkersCount() &&
                            requiredRedMarkers <= GetRedMarkersCount() &&
                            requiredWhiteMarkers <= GetWhiteMarkersCount())
                            ||
                            (requiredBlackMarkers <= GetBlackMarkersCount() &&
                            requiredBlueMarkers <= GetBlueMarkersCount() &&
                            requiredGreenMarkers <= GetGreenMarkersCount() + 1 &&
                            requiredRedMarkers <= GetRedMarkersCount() &&
                            requiredWhiteMarkers <= GetWhiteMarkersCount())
                            ||
                            (requiredBlackMarkers <= GetBlackMarkersCount() &&
                            requiredBlueMarkers <= GetBlueMarkersCount() &&
                            requiredGreenMarkers <= GetGreenMarkersCount() &&
                            requiredRedMarkers <= GetRedMarkersCount() + 1 &&
                            requiredWhiteMarkers <= GetWhiteMarkersCount())
                            ||
                            (requiredBlackMarkers <= GetBlackMarkersCount() &&
                            requiredBlueMarkers <= GetBlueMarkersCount() &&
                            requiredGreenMarkers <= GetGreenMarkersCount() &&
                            requiredRedMarkers <= GetRedMarkersCount() &&
                            requiredWhiteMarkers <= GetWhiteMarkersCount() + 1)
                            );
                    case 0:
                    default:
                        return false;
                }
            }
        }
    }
}
