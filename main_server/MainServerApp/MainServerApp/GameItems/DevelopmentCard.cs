﻿using MainServerApp.Communication.DataTypes;
using MainServerApp.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainServerApp.GameItems
{
    class DevelopmentCard
    {
        private DevelopmentCardTier Tier;
        private string Id;
        private DevelopmentCardBackground Background;
        private int Prestige;
        private MarkerColor ProducedMarker;
        private Dictionary<MarkerColor, int> ConsumedMarkers;

        public DevelopmentCard(DevelopmentCardTier tier, string id, string background, int prestige, string producedMarker)
        {
            Tier = tier;
            Id = id;
            Background = DevelopmentCardBackground.UNKNOWN; // TODO Actually use this
            Prestige = prestige;
            ProducedMarker = EnumUtils.GetEnumValueFromName<MarkerColor>(producedMarker);

            ConsumedMarkers = new Dictionary<MarkerColor, int>();
        }

        public void AddConsumedMarker(string name, int quantity)
        {
            MarkerColor marker = EnumUtils.GetEnumValueFromName<MarkerColor>(name);
            if(!ConsumedMarkers.ContainsKey(marker))
            {
                ConsumedMarkers.Add(marker, quantity);
            }
            else
            {
                Console.WriteLine(String.Format("[ERROR] Consumed markers duplicate for card {0}", Id));
            }
        }

        public MarkerColor GetProducedMarker()
        {
            return ProducedMarker;
        }

        public DevelopmentCardEntity GetEntity()
        {
            DevelopmentCardEntity outEntity = new DevelopmentCardEntity(Tier, Id, Prestige, ProducedMarker);

            foreach (KeyValuePair<MarkerColor, int> marker in ConsumedMarkers)
            {
                outEntity.AddConsumedMarker(marker.Key, marker.Value);
            }

            return outEntity;
        }

        public string GetCardId()
        {
            return Id;
        }

        public int GetCardTierIdx()
        {
            return (int)Tier;
        }

        public Dictionary<MarkerColor, int> GetConsumedMarkers()
        {
            return ConsumedMarkers;
        }
        public bool Equals(DevelopmentCard other)
        {
            return null != other && Id.Equals(other.Id);
        }
        public override bool Equals(object obj)
        {
            return Equals(obj as DevelopmentCard);
        }
        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}
