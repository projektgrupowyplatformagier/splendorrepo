﻿using MainServerApp.Communication.DataTypes;
using MainServerApp.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainServerApp.GameItems
{
    class AristocratCard
    {
        private string Id;
        private AristoCratCardBackground Background;
        private int Prestige;
        public Dictionary<MarkerColor, int> RequiredBonuses { get; }

        public AristocratCard(string id, string background, int prestige)
        {
            Id = id;
            Background = AristoCratCardBackground.UNKNOWN; // TODO Actually use this
            Prestige = prestige;

            RequiredBonuses = new Dictionary<MarkerColor, int>();
        }

        public void AddRequiredBonus(string name, int quantity)
        {
            MarkerColor marker = EnumUtils.GetEnumValueFromName<MarkerColor>(name);
            if (!RequiredBonuses.ContainsKey(marker))
            {
                RequiredBonuses.Add(marker, quantity);
            }
            else
            {
                Console.WriteLine(String.Format("[ERROR] Required markers duplicate for aristocrat {0}", Id));
            }
        }

        public bool AreRequirementsMet(Dictionary<MarkerColor, int> ownedBonuses)
        {
            bool requirementsMet = true;

            foreach (KeyValuePair<MarkerColor, int> requiredBonus in RequiredBonuses)
            {
                if (ownedBonuses.ContainsKey(requiredBonus.Key))
                {
                    if (ownedBonuses[requiredBonus.Key] < requiredBonus.Value)
                    {
                        requirementsMet = false;
                        break;
                    }
                }
                else
                {
                    requirementsMet = false;
                    break;
                }
            }

            return requirementsMet;
        }

        public AristocratCardEntity GetEntity()
        {
            AristocratCardEntity outEntity = new AristocratCardEntity(Id, Prestige);

            foreach (KeyValuePair<MarkerColor, int> bonus in RequiredBonuses)
            {
                outEntity.AddRequiredBonus(bonus.Key, bonus.Value);
            }

            return outEntity;
        }
        public bool Equals(AristocratCard other)
        {
            return null != other && Id.Equals(other.Id);
        }
        public override bool Equals(object obj)
        {
            return Equals(obj as AristocratCard);
        }
        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}
    
