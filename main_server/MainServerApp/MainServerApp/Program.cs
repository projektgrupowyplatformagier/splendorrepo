﻿using MainServerApp.GameItems;
using MainServerApp.Managers;
using MainServerApp.WebServices;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Security.Cryptography.X509Certificates;
using WebSocketSharp;
using WebSocketSharp.Net;
using WebSocketSharp.Server;

namespace MainServerApp
{
    public class Program
    {
        public static void Main(string[] args)
        {
            DatabaseManager.Initialize();

            BotServerManager.AddBotServer("ws://127.0.0.1:6767/BotReq");
            GameManager.Initialize();

            WebSocketServer wssv = new WebSocketServer(4649);
            LobbyManager lobbyManager = new LobbyManager(wssv);

            #region WebSocket lib comments
            // Create a new instance of the WebSocketServer class.
            //
            // If you would like to provide the secure connection, you should create the instance with
            // the 'secure' parameter set to true, or the wss scheme WebSocket URL.
            /* To provide the HTTP Authentication (Basic/Digest).
            wssv.AuthenticationSchemes = AuthenticationSchemes.Basic;
            wssv.Realm = "WebSocket Test";
            wssv.UserCredentialsFinder = id => {
              var name = id.Name;

              // Return user name, password, and roles.
              return name == "nobita"
                     ? new NetworkCredential (name, "password", "gunfighter")
                     : null; // If the user credentials aren't found.
            };
             */

            // Not to remove the inactive sessions periodically.
            //wssv.KeepClean = false;

            // To resolve to wait for socket in TIME_WAIT state.
            //wssv.ReuseAddress = true;

            // Add the WebSocket services.
            #endregion

            wssv.AddWebSocketService<Echo>("/Echo");
            wssv.AddWebSocketService<Lobby>("/Lobby", lobbyManager.CreateLobby);

            wssv.Start();

            if (wssv.IsListening)
            {
                Console.WriteLine("Listening on port {0}, and providing WebSocket services:", wssv.Port);
                foreach (var path in wssv.WebSocketServices.Paths)
                    Console.WriteLine("- {0}", path);
            }

            Console.WriteLine("\nPress Enter key to stop the server...");
            Console.ReadLine();

            wssv.Stop();
        }
    }
}
